# Java Projekt

### Allgemein

 Java Projektentwurf von:
- Andreas Greif
- Florian Jänisch
- Enes Sen

JavaDoc-Dokumentation:
./documentation/index.html

#### Vorbereitungen

Aufrufen des Links: https://gitlab.com/jaenisf/swe_java_simulation_pandemie

Download des Repositories:

![Download des Repositories](README/download_des_repositories.png)

Entpacken des Downloads

Öffnen von Eclipse

#### Innerhalb von Eclipse

Klick auf File -> Open Projects From File System...:

![Open Projects From File System](README/open_projects_from_file_system.png)

Klick auf Directory...:

![Klick auf Directory](README/klick_auf_directory.png)

Auswählen des entpackten Ordners und Auswahl der beiden gefundenen Einträge:

![Import des Projekts](README/import_des_projekts.png)

Klick auf Finish

Nun erscheint das Projekt im Projekt Explorer:

![Projekt Explorer](README/projekt_explorer.png)

Nun Klick auf Window -> Preferences:

![Preferences](README/preferences.png)

Im Anschluss klick auf Java ---> Installed JREs:

![installed jres](README/installed_jres.png)

Klick auf Search

Nun Auswahl des Ordners jdk-14.0.1 im Projektverzeichnis:

![Projektverzeichnis](README/projektverzeichnis.png)

Nun steht das neue jdk zur Verfügung. Das neue jdk auswählen:

![neue jdk](README/neue_jdk.png)

Klick auf Apply and Close

Nun klick auf "pandemie-simulator":

![pandemie-simulator](README/pandemie_simulator.png)

Nun klick auf "run as":

![run as](README/run_as.png)

Auswahl von "Java Application":

![Java Application](README/java_application.png)

Die Auswahl mit OK bestätigen

Im nun erscheinenden Fenster "App" auswählen und Klick auf ok:

![App](README/app.png)

Klick auf OK

Nun erscheint der Simulator

![Simulator](README/simulator.png)

Zu beachten:

- Sehr selten trat der Effekt auf, dass die Simulation von alleine und unerwartet, also nicht am
Ende (keine Infizierten mehr), pausierte.
    - Wir vermuten, dass es etwas mit dem Threading zu tun hat.
    - Wir konnten das Problem aufgrund der fehlenden Reproduzierbarkeit durch die Seltenheit nicht genauer spezifizieren.
    - Sollte dieses Problem auftreten, empfehlen wir das manuelle Pausieren und Starten der Simulation.