/**
 * The package.
 */
package de.dhbw_heidenheim.pandemie_simulator;

/**
 * Die Hauptklasse der Anwendung
 * Stellt die Main-Methode bereit
 */
public class App
{
	
	/** 
	 * Die Konstante raster_breite.
	 */
	private static final int raster_breite = 130;
	
	/** 
	 * Die Konstante raster_hoehe.
	 */
	private static final int raster_hoehe = 106;
	
	/** 
	 * Die Konstente rasterpunkt_groeße.
	 */
	private static final int rasterpunkt_groeße = 7;
	
	/** 
	 * Die Konstante simulation_geschwindigkeit.
	 */
	private static final int simulation_geschwindigkeit = 1;
	
	/**
	 * Die Main Methode
	 *
	 * @param args
	 */
	public static void main(String[] args)
    {
		//Die Instantierung eines neuen Simulationsobjektes der Klasse Simulation.
		Simulation simulation = new Simulation(raster_breite, raster_hoehe, rasterpunkt_groeße, simulation_geschwindigkeit);
		
		//Hiermit wird die zuvor instantierte Simulation gestartet.
		simulation.start();
    }
}