package de.dhbw_heidenheim.pandemie_simulator.view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSlider;

import org.jfree.data.category.DefaultCategoryDataset;

import de.dhbw_heidenheim.pandemie_simulator.model.Health;
import de.dhbw_heidenheim.pandemie_simulator.model.Krankheit;
import de.dhbw_heidenheim.pandemie_simulator.model.Statistik;

/**
 * View-Klasse
 */
public class View 
{
	
	/** Das JFrame_window */
	private JFrame JFrame_window = null;
	
	/** Das JLabel_ueberschrift */
	private JLabel JLabel_ueberschrift = null;
	
	/** 
	 * Das grafische Raster der Simulation
	 */
	private Raster raster = null;
	
	/** 
	 *  Rasterbreite
	 */
	private int raster_breite = 0;
	
	/** 
	 *  Rasterhöhe 
	 */
	private int raster_hoehe = 0;
	
	/** 
	 *  Skalierung eines Rasterpunkts. 
	 */
	private int rasterpunkt_groeße = 0;
	
	/** Knopf zum starten */
	private JButton JButton_starten = null;
	
	/** Knopf zum pausieren */
	private JButton JButton_pausieren = null;
	
	/** Knopf zum stoppen */
	private JButton JButton_stoppen = null;
	
	/** Slider für die Geschwindigkeit */
	private JSlider JSlider_geschwindigkeit = null;
	
	/** Slider für die Anzahl der Personen */
	private JSlider JSlider_personen = null;
	
	/** Der Graph für die Gesunden */
	private Funktionsgraph funktionsgraph_gesund = null;
	
	/** Der Graph für die Kranken */
	private Funktionsgraph funktionsgraph_infiziert = null;
	
	/** Der Graph für die Immunen */
	private Funktionsgraph funktionsgraph_immun = null;
	
	/** Der Graph für die Toten */
	private Funktionsgraph funktionsgraph_tot = null;
	
	/** Kennzahl für die Morbidität */
	private JLabel JLabel_morbiditaet = null;
	
	/** Kennzahl für die Letalität */
	private JLabel JLabel_letalitaet = null;
	
	/** Legende */
	private JButton JButton_gesund = null;
	private JButton JButton_infiziert = null;
	private JButton JButton_infiziert_gesund = null;
	private JButton JButton_immun = null;
	private JButton JButton_immun_gesund = null;
	private JButton JButton_tot = null;
	private JButton JButton_tot_gesund = null;
	
	
	/**
	 * Konstruktor der Klasse
	 *
	 * @param raster_breite: breite
	 * @param raster_hoehe: höhe the raster hoehe
	 * @param rasterpunkt_groeße: Größe des Rasterpunktes
	 */
	public View (int raster_breite, int raster_hoehe, int rasterpunkt_groeße)
	{
		this.setRaster_breite(raster_breite);
		this.setRaster_hoehe(raster_hoehe);
		this.setRasterpunkt_groeße(rasterpunkt_groeße);
	}
	
	/**
	 * Nimmt die Initialisierungen vor
	 */
	public void initialize()
	{
		// JFrame
		this.initializeJFrame_window();
		
		// JLabel - Überschrift
		this.initializeJLabel_ueberschrift();
		
		// Raster
		this.initializeRaster();
		
		// JButton
		this.initializeJButton_starten();
		this.initializeJButton_pausieren();
		this.initializeJButton_stoppen();
		
		// JSlider
		this.initializeJSlider_geschwindigkeit();
		this.initializeJSlider_personen();
		
		// Funktionsgraphen
		this.initializeFunktionsgraph_gesund();
		this.initializeFunktionsgraph_infiziert();
		this.initializeFunktionsgraph_immun();
		this.initializeFunktionsgraph_tot();
		
		// JLabel - Kennzahlen
		this.initializeJLabel_morbiditaet();
		this.initializeJLabel_letalitaet();
		
		// JButton - Legende
		this.initializeLegende();
		
		// JFrame
		this.initializeJFrame_window_setVisible();
		this.getJFrame_window().repaint();
	}
	
	/**
	 * Diese Methode Resettet den View und setzt ihn auf den Startzustand zurück
	 */
	public void reset()
	{
		this.getRaster().RasterUmfärben(Color.WHITE);
		this.setJButton_enabled(true, false, false);
	    this.getJSlider_personen().setEnabled(true);
	    this.resetFunktionsgraph();
	    this.resetKennzahlen();
	}

	/**
	 * Gibt das JFrame_window zurück
	 *
	 * @return gibt das JFrame_window zurück
	 */
	private JFrame getJFrame_window() 
	{
		return JFrame_window;
	}

	/**
	 * Setzt das JFrame_window
	 *
	 * @param JFrame_window: JFrame das gesetzt werden soll
	 */
	private void setJFrame_window(JFrame JFrame_window) 
	{
		this.JFrame_window = JFrame_window;
	}
	
	/**
	 * Initialisiert das JFrame_window
	 */
	private void initializeJFrame_window() 
	{
		this.setJFrame_window(new JFrame("Simulation"));
		this.getJFrame_window().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.getJFrame_window().setLocationRelativeTo(null);
		this.getJFrame_window().getContentPane().setBackground(Color.WHITE);
		//this.getJFrame_window().setMaximumSize(new Dimension(1500, 1000));
		//this.getJFrame_window().setMinimumSize(new Dimension(1500, 1000));
		this.getJFrame_window().setResizable(false);
		this.getJFrame_window().pack();
		this.getJFrame_window().setBounds(0, 0, 1500, 920);
		this.getJFrame_window().setSize(1500, 920);
		this.getJFrame_window().setLayout(null);
		this.getJFrame_window().setVisible(true);
	}
	
	/**
	 * Zeigt das JFrame an
	 */
	private void initializeJFrame_window_setVisible() 
	{
		this.getJFrame_window().setVisible(true);
	}

	/**
	 * Gibt das JLabel_ueberschrift zurück
	 *
	 * @return gibt das JLabel_ueberschrift zurück
	 */
	private JLabel getJLabel_ueberschrift() 
	{
		return JLabel_ueberschrift;
	}

	/**
	 * Setzt das JLabel_ueberschrift
	 *
	 * @param JLabel_ueberschrift: JLabel welches gesetzt werden soll
	 */
	private void setJLabel_ueberschrift(JLabel JLabel_ueberschrift) 
	{
		this.JLabel_ueberschrift = JLabel_ueberschrift;
	}
	
	/**
	 * Initialisiert das Überschrifts-Label
	 */
	private void initializeJLabel_ueberschrift() 
	{
		this.setJLabel_ueberschrift(new JLabel("Pandemie-Simulation"));
		this.getJLabel_ueberschrift().setSize(500, 40);
		this.getJLabel_ueberschrift().setLocation(20, 10);
		this.getJLabel_ueberschrift().setFont(new Font(this.getJLabel_ueberschrift().getFont().getName(), Font.PLAIN, 40));
		this.getJFrame_window().add(this.getJLabel_ueberschrift());
	}

	/**
	 * Gibt das raster zurück
	 *
	 * @return gibt das raster zurück
	 */
	public Raster getRaster() 
	{
		return raster;
	}

	/**
	 * Setzt das raster
	 *
	 * @param raster: welches gesetzt werden soll
	 */
	private void setRaster(Raster raster) 
	{
		this.raster = raster;
	}
	
	/**
	 * Initialisiert das Raster
	 */
	private void initializeRaster() 
	{
		this.setRaster(new Raster(this.getRaster_breite(), this.getRaster_hoehe(), this.getRasterpunkt_groeße()));
		this.getRaster().setLocation(20, 60);
		this.getJFrame_window().add(this.getRaster());
	}
	
	/**
	 * Gibt die raster_breite zurück
	 *
	 * @return gibt die raster_breite zurück
	 */
	private int getRaster_breite() 
	{
		return raster_breite;
	}

	/**
	 * Setzt die raster_breite
	 *
	 * @param raster_breite: raster_breite welche gesetzt werden soll
	 */
	private void setRaster_breite(int raster_breite) 
	{
		this.raster_breite = raster_breite;
	}

	/**
	 * Gibt die raster_hoehe zurück
	 *
	 * @return gibt die raster_hoehe zurück
	 */
	private int getRaster_hoehe() 
	{
		return raster_hoehe;
	}

	/**
	 * Setzt die Rasterhöhe
	 *
	 * @param raster_hoehe: raster_hoehe die gesetzt werden soll
	 */
	private void setRaster_hoehe(int raster_hoehe) 
	{
		this.raster_hoehe = raster_hoehe;
	}

	/**
	 * Gibt die rasterpunkt_groeße zurück
	 *
	 * @return gibt die rasterpunkt_groeße zurück
	 */
	private int getRasterpunkt_groeße() 
	{
		return rasterpunkt_groeße;
	}

	/**
	 * Setzt die rasterpunkt_groeße
	 *
	 * @param rasterpunkt_groeße: rasterpunkt_groeße die gesetzt werden soll
	 */
	private void setRasterpunkt_groeße(int rasterpunkt_groeße) 
	{
		this.rasterpunkt_groeße = rasterpunkt_groeße;
	}

	/**
	 * Gibt den JButton_starten zurück
	 *
	 * @return Gibt den JButton_starten zurück
	 */
	public JButton getJButton_starten() 
	{
		return JButton_starten;
	}

	/**
	 * Setzt den JButton_starten
	 *
	 * @param JButton_starten: JButton_starten der gesetzt werden soll
	 */
	private void setJButton_starten(JButton JButton_starten) 
	{
		this.JButton_starten = JButton_starten;
	}
	
	/**
	 * Initialisiert den JButton_starten
	 */
	private void initializeJButton_starten() 
	{
		this.setJButton_starten(new JButton("Starten"));
		this.getJButton_starten().setSize(120, 30);
		this.getJButton_starten().setLocation(500, 15);
		this.getJFrame_window().add(this.getJButton_starten());
	}

	/**
	 * Gibt den JButton_pausieren zurück
	 *
	 * @return Gibt den JButton_pausieren zurück
	 */
	public JButton getJButton_pausieren() 
	{
		return JButton_pausieren;
	}

	/**
	 * Setzt den JButton pausieren
	 *
	 * @param JButton_pausieren: JButton_pausieren der gesetzt werden soll
	 */
	private void setJButton_pausieren(JButton JButton_pausieren) 
	{
		this.JButton_pausieren = JButton_pausieren;
	}
	
	/**
	 * Initialisiert den JButton_pausieren
	 */
	private void initializeJButton_pausieren() 
	{
		this.setJButton_pausieren(new JButton("Pausieren"));
		this.getJButton_pausieren().setSize(120, 30);
		this.getJButton_pausieren().setLocation(700, 15);
		this.getJFrame_window().add(this.getJButton_pausieren());
	}

	/**
	 * Gibt den JButton_stoppen zurück
	 *
	 * @return Gibt den JButton_stoppen zurück
	 */
	public JButton getJButton_stoppen() 
	{
		return JButton_stoppen;
	}

	/**
	 * Setzt den JButton_stoppen
	 *
	 * @param JButton_stoppen: JButton_stoppen der gesetzt werden soll
	 */
	private void setJButton_stoppen(JButton JButton_stoppen) 
	{
		this.JButton_stoppen = JButton_stoppen;
	}
	
	/**
	 * Initialisiert den JButton_stoppen
	 */
	private void initializeJButton_stoppen() 
	{
		this.setJButton_stoppen(new JButton("Stoppen & Reset"));
		this.getJButton_stoppen().setSize(140, 30);
		this.getJButton_stoppen().setLocation(900, 15);
		this.getJFrame_window().add(this.getJButton_stoppen());
	}
	
	/**
	 * Setzt die Buttons auf Aktiv oder Inaktiv
	 *
	 * @param jbutton_starten: Startknopf
	 * @param jbutton_pausieren: Pausieren-Knopf
	 * @param jbutton_stoppen: Stopp-Knopf
	 */
	public void setJButton_enabled(boolean jbutton_starten, boolean jbutton_pausieren, boolean jbutton_stoppen)
	{
		this.getJButton_starten().setEnabled(jbutton_starten);
		this.getJButton_pausieren().setEnabled(jbutton_pausieren);
		this.getJButton_stoppen().setEnabled(jbutton_stoppen);
	}
	
	/**
	 * Gibt den JSlider_geschwindigkeit zurück
	 *
	 * @return Gibt den JSlider_geschwindigkeit zurück
	 */
	public JSlider getJSlider_geschwindigkeit() 
	{
		return JSlider_geschwindigkeit;
	}

	/**
	 * Setzt den JSlider_geschwindigkeit
	 *
	 * @param jSlider_geschwindigkeit: JSlider_geschwindigkeit der gesetzt werden soll
	 */
	private void setJSlider_geschwindigkeit(JSlider jSlider_geschwindigkeit) 
	{
		JSlider_geschwindigkeit = jSlider_geschwindigkeit;
	}
	
	/**
	 * Initialisiert den JSlider_geschwindigkeit
	 */
	private void initializeJSlider_geschwindigkeit()
	{
		//Größe und Position
		this.setJSlider_geschwindigkeit(new JSlider());
		this.getJSlider_geschwindigkeit().setSize(300, 60);
		this.getJSlider_geschwindigkeit().setLocation(1100, 15);
	
		//Range: Minimal 1 Tick in der Sekunde & Maximal 4 Ticks in der Sekunde
		this.getJSlider_geschwindigkeit().setMinimum(1);
		this.getJSlider_geschwindigkeit().setMaximum(101);
		this.getJSlider_geschwindigkeit().setValue(51);
	
		//Rahmen und Tickmarkierungen am Slider
		this.getJSlider_geschwindigkeit().setBorder(BorderFactory.createTitledBorder("Geschwindigkeitsfaktor"));
		this.getJSlider_geschwindigkeit().setPaintTicks(true);
		this.getJSlider_geschwindigkeit().setPaintLabels(true);
		this.getJSlider_geschwindigkeit().setMajorTickSpacing(10);
		this.getJSlider_geschwindigkeit().setMinorTickSpacing(5);
	
		this.getJFrame_window().add(this.getJSlider_geschwindigkeit());
	}

	/**
	 * Gibt den JSlider_personen zurück
	 *
	 * @return Gibt den JSlider_personen zurück
	 */
	public JSlider getJSlider_personen() 
	{
		return JSlider_personen;
	}

	/**
	 * Setzt den jSlider_personen
	 *
	 * @param jSlider_personen: jSlider_personen der gesetzt werden soll
	 */
	private void setJSlider_personen(JSlider jSlider_personen) 
	{
		JSlider_personen = jSlider_personen;
	}
	
	/**
	 * Initialisiert den JSlider_personen
	 */
	private void initializeJSlider_personen()
	{
		//Größe und Position
		this.setJSlider_personen(new JSlider());
		this.getJSlider_personen().setSize(300, 60);
		this.getJSlider_personen().setLocation(1100, 80);
	
		//Range: Minimal 2 Personen & Maximal 80% von der Anzahl der Rasterpunkte
		this.getJSlider_personen().setMinimum(2);
		this.getJSlider_personen().setMaximum(this.getRaster_breite() * this.getRaster_hoehe());
		this.getJSlider_personen().setValue((int) (this.getJSlider_personen().getMaximum() * 0.5) + this.getJSlider_personen().getMinimum());
	
		//Rahmen und Tickmarkierungen am Slider
		this.getJSlider_personen().setBorder(BorderFactory.createTitledBorder("Anzahl der Personen"));
		this.getJSlider_personen().setPaintTicks(true);
		this.getJSlider_personen().setPaintLabels(true);
		this.getJSlider_personen().setMajorTickSpacing(2500);
		this.getJSlider_personen().setMinorTickSpacing(500);
	
		this.getJFrame_window().add(this.getJSlider_personen());
	}
	
	/**
	 * Setzt die Slider entweder auf Aktiv oder Inaktiv
	 *
	 * @param jslider_geschwindigkeit: Geschwindigkeits-Slider
	 * @param jslider_personen: Personen-Slider
	 */
	public void setJSlider_enabled(boolean jslider_geschwindigkeit, boolean jslider_personen)
	{
		this.getJSlider_geschwindigkeit().setEnabled(jslider_geschwindigkeit);
		this.getJSlider_personen().setEnabled(jslider_personen);
	}

	/**
	 * Gibt den funktionsgraph_gesund zurück
	 *
	 * @return Gibt den funktionsgraph_gesund zurück
	 */
	private Funktionsgraph getFunktionsgraph_gesund() 
	{
		return funktionsgraph_gesund;
	}

	/**
	 * Setzt den funktionsgraph_gesund
	 *
	 * @param funktionsgraph_gesund: funktionsgraph_gesund der gesetzt werden soll
	 */
	private void setFunktionsgraph_gesund(Funktionsgraph funktionsgraph_gesund) 
	{
		this.funktionsgraph_gesund = funktionsgraph_gesund;
	}
	
	/**
	 * Initialisiert den funktionsgraph_gesund
	 */
	private void initializeFunktionsgraph_gesund() 
	{
		this.setFunktionsgraph_gesund(new Funktionsgraph(new DefaultCategoryDataset(), Health.GESUND.toString()));
		this.getFunktionsgraph_gesund().setSize(250, 250);
		this.getFunktionsgraph_gesund().setLocation(950, 160);
		this.getJFrame_window().add(this.getFunktionsgraph_gesund());
	}
	
	/**
	 * Resettet den funktionsgraph_gesund
	 */
	private void resetFunktionsgraph_gesund() 
	{
		this.getJFrame_window().remove(this.getFunktionsgraph_gesund());
		this.initializeFunktionsgraph_gesund();
		this.getJFrame_window().repaint();
	}

	/**
	 * Gibt den funktionsgraph_infiziert zurück
	 *
	 * @return Gibt den funktionsgraph_infiziert zurück
	 */
	private Funktionsgraph getFunktionsgraph_infiziert() 
	{
		return funktionsgraph_infiziert;
	}

	/**
	 * Setzt den funktionsgraph_infiziert
	 *
	 * @param funktionsgraph_infiziert: funktionsgraph_infiziert der gesetzt werden soll
	 */
	private void setFunktionsgraph_infiziert(Funktionsgraph funktionsgraph_infiziert) 
	{
		this.funktionsgraph_infiziert = funktionsgraph_infiziert;
	}
	
	/**
	 * Initialisiert den funktionsgraph_infiziert
	 */
	private void initializeFunktionsgraph_infiziert() 
	{
		this.setFunktionsgraph_infiziert(new Funktionsgraph(new DefaultCategoryDataset(), Health.INFIZIERT.toString()));
		this.getFunktionsgraph_infiziert().setSize(250, 250);
		this.getFunktionsgraph_infiziert().setLocation(1200, 160);
		this.getJFrame_window().add(this.getFunktionsgraph_infiziert());
	}
	
	/**
	 * Resettet den funktionsgraph_infiziert
	 */
	private void resetFunktionsgraph_infiziert() 
	{
		this.getJFrame_window().remove(this.getFunktionsgraph_infiziert());
		this.initializeFunktionsgraph_infiziert();
		this.getJFrame_window().repaint();
	}

	/**
	 * Gibt den funktionsgraph_immun zurück
	 *
	 * @return Gibt den funktionsgraph_immun zurück
	 */
	private Funktionsgraph getFunktionsgraph_immun() 
	{
		return funktionsgraph_immun;
	}

	/**
	 * Setzt den Funktionsgraph Immun
	 *
	 * @param funktionsgraph_immun: funktionsgraph_immun der gesetzt werden soll
	 */
	private void setFunktionsgraph_immun(Funktionsgraph funktionsgraph_immun) 
	{
		this.funktionsgraph_immun = funktionsgraph_immun;
	}
	
	/**
	 * Initialisiert den funktionsgraph_immun
	 */
	private void initializeFunktionsgraph_immun() 
	{
		this.setFunktionsgraph_immun(new Funktionsgraph(new DefaultCategoryDataset(), Health.IMMUN.toString()));
		this.getFunktionsgraph_immun().setSize(250, 250);
		this.getFunktionsgraph_immun().setLocation(950, 420);
		this.getJFrame_window().add(this.getFunktionsgraph_immun());
	}
	
	/**
	 * Resettet den Funktionsgraph Immun
	 */
	private void resetFunktionsgraph_immun() 
	{
		this.getJFrame_window().remove(this.getFunktionsgraph_immun());
		this.initializeFunktionsgraph_immun();
		this.getJFrame_window().repaint();
	}

	/**
	 * Gibt den funktionsgraph_tot zurück
	 *
	 * @return Gibt den funktionsgraph_tot zurück
	 */
	private Funktionsgraph getFunktionsgraph_tot() 
	{
		return funktionsgraph_tot;
	}

	/**
	 * Setzt den funktionsgraph_tot
	 *
	 * @param funktionsgraph_tot: funktionsgraph_tot der gesetzt werden soll
	 */
	private void setFunktionsgraph_tot(Funktionsgraph funktionsgraph_tot) 
	{
		this.funktionsgraph_tot = funktionsgraph_tot;
	}
	
	/**
	 * Initialisiert den funktionsgraph_tot
	 */
	private void initializeFunktionsgraph_tot() 
	{
		this.setFunktionsgraph_tot(new Funktionsgraph(new DefaultCategoryDataset(), Health.TOT.toString()));
		this.getFunktionsgraph_tot().setSize(250, 250);
		this.getFunktionsgraph_tot().setLocation(1200, 420);
		this.getJFrame_window().add(this.getFunktionsgraph_tot());
	}
	
	/**
	 * Resettet den funktionsgraph_tot
	 */
	private void resetFunktionsgraph_tot() 
	{
		this.getJFrame_window().remove(this.getFunktionsgraph_tot());
		this.initializeFunktionsgraph_tot();
		this.getJFrame_window().repaint();
	}
	
	/**
	 * Setzt die Funktionsgraphen
	 *
	 * @param krankheit: Objekt der Klasse Krankheit
	 */
	public void setFunktionsgraph(Krankheit krankheit) 
	{
		this.getFunktionsgraph_gesund().setDataset(krankheit, Health.GESUND);
		this.getFunktionsgraph_infiziert().setDataset(krankheit, Health.INFIZIERT);
		this.getFunktionsgraph_immun().setDataset(krankheit, Health.IMMUN);
		this.getFunktionsgraph_tot().setDataset(krankheit, Health.TOT);
	}

	/**
	 * Resettet die Funktionsgraphen
	 */
	public void resetFunktionsgraph() 
	{
		this.resetFunktionsgraph_gesund();
		this.resetFunktionsgraph_infiziert();
		this.resetFunktionsgraph_immun();
		this.resetFunktionsgraph_tot();
	}
	
	/**
	 * Gibt das JLabel_morbiditaet zurück
	 *
	 * @return Gibt das JLabel_morbiditaet zurück
	 */
	private JLabel getJLabel_morbiditaet() 
	{
		return JLabel_morbiditaet;
	}

	/**
	 * Setzt das jLabel_morbiditaet
	 *
	 * @param jLabel_morbiditaet: jLabel_morbiditaet welches gesetzt werden soll
	 */
	private void setJLabel_morbiditaet(JLabel jLabel_morbiditaet) 
	{
		JLabel_morbiditaet = jLabel_morbiditaet;
	}
	
	/**
	 * Initialisiert das JLabel_morbiditaet
	 */
	private void initializeJLabel_morbiditaet() 
	{
		this.setJLabel_morbiditaet(new JLabel("Morbidität"));
		this.getJLabel_morbiditaet().setText(this.getJLabel_morbiditaet().getText() + " = Keine Daten");
		this.getJLabel_morbiditaet().setSize(500, 40);
		this.getJLabel_morbiditaet().setLocation(1000, 665);
		this.getJLabel_morbiditaet().setFont(new Font(this.getJLabel_morbiditaet().getFont().getName(), Font.PLAIN, 18));
		this.getJFrame_window().add(this.getJLabel_morbiditaet());
	}
	
	/**
	 * Setzt den Wert, welches das JLabel_morbiditaet anzeigen soll
	 *
	 * @param kennzahlwert: Wert der ausgegeben werden soll
	 */
	private void setJLabel_morbiditaet_value(double kennzahlwert) 
	{
		try
		{
			DecimalFormat df = new DecimalFormat("0.000");
			this.getJLabel_morbiditaet().setText("Morbidität = " + df.format(kennzahlwert) + "%");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	/**
	 * Resettet das JLabel_morbiditaet
	 */
	private void resetJLabel_morbiditaet() 
	{
		this.getJFrame_window().remove(this.getJLabel_morbiditaet());
		this.initializeJLabel_morbiditaet();
		this.getJFrame_window().repaint();
	}

	/**
	 * Gibt das JLabel_letalitaet zurück
	 *
	 * @return Gibt das JLabel_letalitaet zurück
	 */
	private JLabel getJLabel_letalitaet() 
	{
		return JLabel_letalitaet;
	}

	/**
	 * Setzt das JLabel_letalitaet
	 *
	 * @param jLabel_letalitaet: JLabel_letalitaet welches gesetzt werden soll
	 */
	private void setJLabel_letalitaet(JLabel jLabel_letalitaet) 
	{
		JLabel_letalitaet = jLabel_letalitaet;
	}
	
	/**
	 * Initialisiert das JLabel_letalitaet
	 */
	private void initializeJLabel_letalitaet() 
	{
		this.setJLabel_letalitaet(new JLabel("Letalität"));
		this.getJLabel_letalitaet().setText(this.getJLabel_letalitaet().getText() + " = Keine Daten");
		this.getJLabel_letalitaet().setSize(500, 40);
		this.getJLabel_letalitaet().setLocation(1250, 665);
		this.getJLabel_letalitaet().setFont(new Font(this.getJLabel_letalitaet().getFont().getName(), Font.PLAIN, 18));
		this.getJFrame_window().add(this.getJLabel_letalitaet());
	}
	
	/**
	 * Setzt den Wert, welchen das JLabel_letalitaet anzeigen soll
	 *
	 * @param kennzahlwert: Wert der ausgegeben werden soll
	 */
	private void setJLabel_letalitaet_value(double kennzahlwert) 
	{
		try
		{
			DecimalFormat df = new DecimalFormat("0.000");
			this.getJLabel_letalitaet().setText("Letalität = " + df.format(kennzahlwert) + "%");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	/**
	 * Resettet das JLabel_letalitaet
	 */
	private void resetJLabel_letalitaet() 
	{
		this.getJFrame_window().remove(this.getJLabel_letalitaet());
		this.initializeJLabel_letalitaet();
		this.getJFrame_window().repaint();
	}
	
	/**
	 * Setzt die Werte der Kennzahlen-Labels
	 *
	 * @param statistik: Objekt vom Typ Statistik
	 */
	public void setKennzahlen(Statistik statistik) 
	{
		try
		{
			this.setJLabel_letalitaet_value(statistik.berechneLetalitaet());
			this.setJLabel_morbiditaet_value(statistik.berechneMorbiditaet());
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	/**
	 * Resettet die Kennzahlen
	 */
	public void resetKennzahlen() 
	{
		this.resetJLabel_morbiditaet();
		this.resetJLabel_letalitaet();
	}
	
	/**
	 * Initialisiert die Legende
	 */
	private void initializeLegende() 
	{
		//Gesund
		this.JButton_gesund = new JButton("BGesund");
		this.JButton_gesund.setText("Gesund");
		this.JButton_gesund.setSize(100, 30);
		this.JButton_gesund.setLocation(1000, 755);
		this.JButton_gesund.setFont(new Font(this.JButton_gesund.getName(), Font.PLAIN, 18));
		this.JButton_gesund.setBackground(new Color(0, 255, 150));
		//this.JButton_gesund.setEnabled(false);
		this.getJFrame_window().add(this.JButton_gesund);
		
		//Immun
		this.JButton_immun = new JButton("BImmun");
		this.JButton_immun.setText("Immun");
		this.JButton_immun.setSize(100, 30);
		this.JButton_immun.setLocation(1105, 755);
		this.JButton_immun.setFont(new Font(this.JButton_immun.getName(), Font.PLAIN, 18));
		this.JButton_immun.setBackground(new Color(0, 160, 255));
		//this.JButton_immun.setEnabled(false);
		this.getJFrame_window().add(this.JButton_immun);
		
		//Immun und Gesund
		this.JButton_immun_gesund = new JButton("BImmunGesund");
		this.JButton_immun_gesund.setText("Immun & Gesund");
		this.JButton_immun_gesund.setSize(200, 30);
		this.JButton_immun_gesund.setLocation(1210, 755);
		this.JButton_immun_gesund.setFont(new Font(this.JButton_immun_gesund.getName(), Font.PLAIN, 18));
		this.JButton_immun_gesund.setBackground(new Color(41, 241, 251));
		//this.JButton_immun_gesund.setEnabled(false);
		this.getJFrame_window().add(this.JButton_immun_gesund);
		
		//Infiziert
		this.JButton_infiziert = new JButton("BInfiziert");
		this.JButton_infiziert.setText("Infiziert");
		this.JButton_infiziert.setSize(100, 30);
		this.JButton_infiziert.setLocation(1105, 720);
		this.JButton_infiziert.setFont(new Font(this.JButton_infiziert.getName(), Font.PLAIN, 18));
		this.JButton_infiziert.setBackground(new Color(255, 75, 75));
		//this.JButton_infiziert.setEnabled(false);
		this.getJFrame_window().add(this.JButton_infiziert);
		
		//Infiziert und Gesund
		this.JButton_infiziert_gesund = new JButton("BInfiziertGesund");
		this.JButton_infiziert_gesund.setText("Infiziert & Gesund");
		this.JButton_infiziert_gesund.setSize(200, 30);
		this.JButton_infiziert_gesund.setLocation(1210, 720);
		this.JButton_infiziert_gesund.setFont(new Font(this.JButton_infiziert_gesund.getName(), Font.PLAIN, 18));
		this.JButton_infiziert_gesund.setBackground(new Color(255, 255, 0));
		//this.JButton_infiziert_gesund.setEnabled(false);
		this.getJFrame_window().add(this.JButton_infiziert_gesund);
		
		//Tot
		this.JButton_tot = new JButton("BTot");
		this.JButton_tot.setText("Tot");
		this.JButton_tot.setSize(100, 30);
		this.JButton_tot.setLocation(1105, 790);
		this.JButton_tot.setFont(new Font(this.JButton_tot.getName(), Font.PLAIN, 18));
		this.JButton_tot.setBackground(new Color(170, 30, 255));
		//this.JButton_tot.setEnabled(false);
		this.getJFrame_window().add(this.JButton_tot);
		
		//Tot und Gesund
		this.JButton_tot_gesund = new JButton("BTotGesund");
		this.JButton_tot_gesund.setText("Tot & Gesund");
		this.JButton_tot_gesund.setSize(200, 30);
		this.JButton_tot_gesund.setLocation(1210, 790);
		this.JButton_tot_gesund.setFont(new Font(this.JButton_tot_gesund.getName(), Font.PLAIN, 18));
		this.JButton_tot_gesund.setBackground(new Color(255, 200, 0));
		//this.JButton_tot_gesund.setEnabled(false);
		this.getJFrame_window().add(this.JButton_tot_gesund);		
	}
}