package de.dhbw_heidenheim.pandemie_simulator.view;

import org.jfree.chart.*;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import de.dhbw_heidenheim.pandemie_simulator.model.Health;
import de.dhbw_heidenheim.pandemie_simulator.model.Krankheit;

/**
 * Klasse die den Funktionsgraphen repräsentiert.
 * Die Klasse erbt hierbei von ChartPanel
 */
public class Funktionsgraph extends ChartPanel
{
	
	/** Hier sind zu verarbeitenden Daten enthalten */
	private DefaultCategoryDataset dataset = null;
	
	/** Der Titel des Graphen */
	private String title = null;
	
	/** Zählervariable */
	private int counter = 0;
	
	/**
	 * Public Construktor der Klasse
	 *
	 * @param dataset: zu verwendender Datenbestand
	 * @param title: der zu verwendende Titel
	 */
	public Funktionsgraph(DefaultCategoryDataset dataset, String title) 
	{
		super(InitializeFunktiongraph(dataset, title));
		this.setDataset(dataset);
	}

	/**
	 * Initialisieren des Funktionsgraphen
	 *
	 * @param dataset: zu verwendender Datenbestand
	 * @param title: der zu verwendende Titel
	 * @return gibt das neu erstellte Objekt zurück
	 */
	public static JFreeChart InitializeFunktiongraph(DefaultCategoryDataset dataset, String title) 
	{
		dataset.setValue(0, "0", "0");
		
		JFreeChart JFreeChart = ChartFactory.createBarChart(
                title,
                "Ticks",
                "Anzahl der Personen",
                dataset,
                PlotOrientation.VERTICAL,
                false, true, false);
		
		return JFreeChart;
	}

	/**
	 * Gibt den Datenbestand zurück
	 *
	 * @return Datenbestand
	 */
	private DefaultCategoryDataset getDataset() 
	{
		return dataset;
	}

	/**
	 * Setzt den Datenbestand
	 *
	 * @param dataset: zu verwendender Datenbestand
	 */
	private void setDataset(DefaultCategoryDataset dataset) 
	{
		this.dataset = dataset;
	}
	
	/**
	 * Setzt einen Wert im Datensatz
	 *
	 * @param krankheit: objekt der Klasse Krankheit
	 * @param health: spezifisches Health-Element
	 */
	public void setDataset(Krankheit krankheit, Health health) 
	{
		this.getDataset().setValue(krankheit.getData(health).get(krankheit.getData(health).size() - 1), "0", Integer.toString(counter++));
	}

	/**
	 * Gibt den Titel des Graphen zurück
	 *
	 * @return Gibt den Titel zurück
	 */
	private String getTitle() 
	{
		return title;
	}

	/**
	 * Setzt den Titel
	 *
	 * @param title: setzt den Titel
	 */
	private void setTitle(String title) 
	{
		this.title = title;
	}

	/**
	 * Gibt den Counter zurück
	 *
	 * @return gibt den Counter zurück
	 */
	private int getCounter() 
	{
		return counter;
	}

	/**
	 * Setzt den Counter
	 *
	 * @param counter: counter der gesetzt werden muss
	 */
	private void setCounter(int counter) 
	{
		this.counter = counter;
	}
}