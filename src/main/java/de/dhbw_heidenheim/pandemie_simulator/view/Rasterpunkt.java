package de.dhbw_heidenheim.pandemie_simulator.view;

import javax.swing.JButton;

/**
 * Diese Klasse bildet einen Rasterpunkt ab.
 * Die Klasse erbt von JButton
 */
public class Rasterpunkt extends JButton 
{	
	
	/** 
	 * Skalierung des Rasterpunkts.
	 * Rasterpunktbreite.
	 */
	private int breite = 0;
	
	/** 
	 * Skalierung des Rasterpunkts.
	 * Rasterpunkthöhe. 
	 */
	private int hoehe = 0;
	
	/** 
	 * Die X-Koordinate des Rasterpunktes.
	 */
	private int koordinate_x = 0;
	
	/** 
	 * Die Y-Koordinate des Rasterpunktes.
	 */
	private int koordinate_y = 0;
	
	/**
	 * privater Konstruktor der Rasterpunkt Klasse
	 */
	private Rasterpunkt()
	{
		// Setzte Eigenschaften des Rasterpunktes
		this.setEnabled(false);
		this.setText("");
		this.setOpaque(true);
	}
	
	/**
	 * Konstruktor mit den Koordinaten- und Sklierungsangaben.
	 *
	 * @param koordinate_x: x Koordinate
	 * @param koordinate_y: y Koordinate
	 * @param breite: Breite
	 * @param hoehe: Höhe
	 */
	public Rasterpunkt(int koordinate_x, int koordinate_y, int breite, int hoehe)
	{
		// Aufrufen des Konstruktors
		this();
		
		// Setzen der Skalierung
		this.setBreite(breite);
		this.setHoehe(hoehe);
		
		// Setzen der Koordinaten
		this.setKoordinate_x(koordinate_x);
		this.setKoordinate_y(koordinate_y);
		
		// Setzte Eigenschaften des Rasterpunktes
		this.setBounds(this.getKoordinate_x(), this.getKoordinate_y(), this.getBreite(), this.getHoehe());
	}
	
	/**
	 * Gibt die Breite zurück
	 *
	 * @return Gibt die Breite zurück
	 */
	private int getBreite() 
	{
		return breite;
	}

	/**
	 * Setzt die Breite
	 *
	 * @param breite: Breite die gesetzt werden soll
	 */
	private void setBreite(int breite) 
	{
		this.breite = breite;
	}

	/**
	 * Gibt die Höhe zurück
	 *
	 * @return gibt die Höhe zurück
	 */
	private int getHoehe() 
	{
		return hoehe;
	}

	/**
	 * Setzt die Höhe
	 *
	 * @param hoehe: Höhe die gesetzt werden soll
	 */
	private void setHoehe(int hoehe) 
	{
		this.hoehe = hoehe;
	}

	/**
	 * Gibt die koordinate_x zurück
	 *
	 * @return gibt die koordinate_x zurück
	 */
	private int getKoordinate_x() 
	{
		return koordinate_x;
	}

	/**
	 * Setzt die koordinate_x
	 *
	 * @param koordinate_x: koordinate_x die gesetzt werden soll
	 */
	private void setKoordinate_x(int koordinate_x) 
	{
		this.koordinate_x = koordinate_x;
	}

	/**
	 * Gibt die koordinate_y zurück
	 *
	 * @return gibt die koordinate_y zurück
	 */
	private int getKoordinate_y() 
	{
		return koordinate_y;
	}

	/**
	 * Setzt die koordinate_y
	 *
	 * @param koordinate_y: koordinate_y die gesetzt werden soll
	 */
	private void setKoordinate_y(int koordinate_y) 
	{
		this.koordinate_y = koordinate_y;
	}
}