package de.dhbw_heidenheim.pandemie_simulator.view;

import java.awt.Color;

import java.util.List;
import java.util.stream.IntStream;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.plaf.basic.BasicButtonUI;

import de.dhbw_heidenheim.pandemie_simulator.model.*;

/**
 * The Class Raster.
 */
public class Raster extends JPanel 
{
	
	/**
	 *  Skalierung des Rasters.
	 *  Rasterbreite. 
	 */
	private int breite = 0;
	
	/**
	 *  Skalierung des Rasters.
	 *  Rasterhöhe. 
	 */
	private int hoehe = 0;
	
	/**
	 *  Skalierung eines Rasterpunkts. 
	 */
	private int rasterpunkt_groeße = 0;
	
	/** 
	 * Gesamtheit aller Rasterpunkte im grafischen Raster
	 */
	private ArrayList<Rasterpunkt> rasterpunkte = new ArrayList<Rasterpunkt>();
	
	/**
	 * Instanziiert ein neues Raster
	 */
	private Raster()
	{
		// Setzt Eigenschaften des Rasters
		this.setLayout(null);
	}
	
	/**
	 * Instanziiert ein neues Raster
	 *
	 * @param breite: breite des Rasters
	 * @param hoehe: hoehe des Rasters
	 * @param rasterpunkt_groeße: Rasterpunktskalierung
	 */
	public Raster(int breite, int hoehe, int rasterpunkt_groeße)
	{
		// Aufrufen des Konstruktors
		this();
		
		// Setzen der Skalierung
		this.setBreite(breite);
		this.setHoehe(hoehe);
		
		// Setzen der Rasterpunktskalierung
		this.setRasterpunkt_groeße(rasterpunkt_groeße);
		
		// Erzeuge Rasterpunkte für Raster
		this.RasterErzeugen();
		
		// Setzte Eigenschaften des Rasters
		this.setSize((this.getBreite() + 2) * this.getRasterpunkt_groeße(), (this.getHoehe() + 2) * this.getRasterpunkt_groeße());
		this.setBorder(BorderFactory.createLineBorder(Color.black));
	}
	
	/**
	 * Raster erzeugen.
	 */
	private void RasterErzeugen()
	{
		// Erzeuge Rasterpunkte für Raster
		/*for (int i = 1; i <= this.getBreite(); i++)
    	{
    		for (int j = 1; j <= this.getHoehe(); j++)
        	{
    			Rasterpunkt rasterpunkt = new Rasterpunkt(this.getRasterpunkt_groeße() * i, this.getRasterpunkt_groeße() * j, this.getRasterpunkt_groeße(), this.getRasterpunkt_groeße()); 
    	        rasterpunkt.setBackground(Color.WHITE);
    	        this.add(rasterpunkt);
    	        
    	        this.getRasterpunkte().add(rasterpunkt);
        	}
    	}*/
		
		//Baut das Raster auf
		IntStream.range(1, this.getBreite() + 1)
		.parallel()
		.forEach(i -> {
			IntStream.range(1, this.getHoehe() + 1)
				.forEach(j -> {
					Rasterpunkt rasterpunkt = new Rasterpunkt(this.getRasterpunkt_groeße() * i, this.getRasterpunkt_groeße() * j, this.getRasterpunkt_groeße(), this.getRasterpunkt_groeße()); 
	    	        rasterpunkt.setBackground(Color.WHITE);
	    	        this.add(rasterpunkt);
	    	        
	    	        this.getRasterpunkte().add(rasterpunkt);
				});
		});
	}
	
	/**
	 * Raster umfärben.
	 *
	 * @param raster: raster
	 */
	public void RasterUmfärben(List<Person>[][] raster)
	{
		this.getRasterpunkte().parallelStream().forEach(rasterpunkt -> {
			//x ermitteln
			int x = (this.getRasterpunkte().indexOf(rasterpunkt) % this.getBreite());
			//y ermitteln
			int y = ((this.getRasterpunkte().indexOf(rasterpunkt) - x) / this.getBreite());
    		
			if (raster[x][y].isEmpty() == true)
			{
				//Leere Felder auf weiß setzen
				rasterpunkt.setBackground(Color.WHITE);
			}
			else
			{
				//Ermitteln des Gesundheitszustandes und entsprechendes einfärben
				
				long anzahl_gesund = 0;
				long anzahl_infisziert = 0;
				long anzahl_immun = 0;
				long anzahl_tot = 0;
				
				for (Person person : raster[x][y])
				{
					if (person.getHealth() == Health.GESUND)
					{
						anzahl_gesund++;
					}
					
					if (person.getHealth() == Health.INFIZIERT)
					{
						anzahl_infisziert++;
					}
					
					if (person.getHealth() == Health.IMMUN)
					{
						anzahl_immun++;
					}
					
					if (person.getHealth() == Health.TOT)
					{
						anzahl_tot++;
					}
				}
				
				if (anzahl_tot > 0)
				{
					rasterpunkt.setBackground(new Color(170, 30, 255));
					
					if (anzahl_gesund > 0)
					{
						rasterpunkt.setBackground(new Color(255, 200, 0));
					}
				} 
				else if (anzahl_infisziert > 0)
				{
					rasterpunkt.setBackground(new Color(255, 75, 75));
					
					if (anzahl_gesund > 0)
					{
						rasterpunkt.setBackground(new Color(255, 255, 0));
					}
				}
				else if (anzahl_immun > 0)
				{
					rasterpunkt.setBackground(new Color(0, 160, 255));
					
					if (anzahl_gesund > 0)
					{
						rasterpunkt.setBackground(new Color(41, 241, 251));
					}
				}
				else if (anzahl_gesund > 0)
				{
					rasterpunkt.setBackground(new Color(0, 255, 150));
				}
			}
		});
	}
	
	/**
	 * Raster umfärben alle Felder mit der gleichen Farbe
	 *
	 * @param color: Farbe in die umgewandelt werden soll
	 */
	public synchronized void RasterUmfärben(Color color)
	{
		for (Rasterpunkt rasterpunkt : this.getRasterpunkte())
		{
			rasterpunkt.setBackground(color);	
		}
	}

	/**
	 * Gibt die Breite zurück
	 *
	 * @return gibt die Breite zurück
	 */
	protected int getBreite() 
	{
		return breite;
	}

	/**
	 * Setzt die Breite
	 *
	 * @param breite: Breite die gesetzt werden soll
	 */
	private void setBreite(int breite) 
	{
		this.breite = breite;
	}

	/**
	 * Gibt die Höhe zurück
	 *
	 * @return gibt die Höhe zurück
	 */
	protected int getHoehe() 
	{
		return hoehe;
	}

	/**
	 * Setzt die Höhe
	 *
	 * @param hoehe: Höhe die gesetzt werden soll
	 */
	private void setHoehe(int hoehe) 
	{
		this.hoehe = hoehe;
	}

	/**
	 * Gibt die rasterpunkt_groeße zurück
	 *
	 * @return gibt die rasterpunkt_groeße zurück
	 */
	protected int getRasterpunkt_groeße() 
	{
		return rasterpunkt_groeße;
	}

	/**
	 * Setzt die rasterpunkt_groeße
	 *
	 * @param rasterpunkt_groeße: rasterpunkt_groeße die gesetzt werden soll
	 */
	private void setRasterpunkt_groeße(int rasterpunkt_groeße) 
	{
		this.rasterpunkt_groeße = rasterpunkt_groeße;
	}

	/**
	 * Gibt die Rasterpunkte zurück
	 *
	 * @return gibt die Rasterpunkte zurück
	 */
	private ArrayList<Rasterpunkt> getRasterpunkte() 
	{
		return rasterpunkte;
	}

	/**
	 * Setzt die Rasterpunkte
	 *
	 * @param rasterpunkte: rasterpunkte die gesetzt werden sollen
	 */
	private void setRasterpunkte(ArrayList<Rasterpunkt> rasterpunkte) 
	{
		this.rasterpunkte = rasterpunkte;
	}	
}