/**
 * The package.
 */
package de.dhbw_heidenheim.pandemie_simulator.controller;

/**
 * The imports.
 */
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.dhbw_heidenheim.pandemie_simulator.model.Health;

/**
 * Die Klasse repräsentiert den ControllerThread
 * Die Klasse erbt von Thread 
 * @see Thread
 */
public class ControllerThread extends Thread
{
	
	/** 
	 * Simulationsstatus: 
	 * false bedeutet passiv und true bedeutet aktiv
	 */
	private boolean simulation_status = false;
	
	/** 
	 * Simulationsausführungsstatus: 
	 * false bedeutet keine Ausführung und true bedeutet Ausführung der Simulation 
	 */
	private boolean simulation_ausfuehrungsstatus = false;
	
	/**
	 * Die ist die Simulationsgeschwindigkeit der Simulation, 
	 * die während der Ausführung verändert werden kann.
	 */
	private int simulation_geschwindigkeit = 0;
	
	/** 
	 * Dies ist der Controller der Simulation. 
	 */
	private Controller controller = null;
	
	/**
	 * Construktor der Klasse
	 *
	 * @param controller: Der Controller
	 * @param simulationsgeschwindigkeit: Die Simulationsgeschwindigkeit
	 */
	public ControllerThread(Controller controller, int simulationsgeschwindigkeit)
	{
		//Setzt die Priorität dieses Threads auf die maximale Priorität.
		this.setPriority(MAX_PRIORITY);
		
		//Setzen des Controllers, damit der ControllerThread den Controller kennt.
		this.setController(controller);
		
		//Spezifizieren der Simulationsgeschwindigkeit.
		this.setSimulation_geschwindigkeit(simulationsgeschwindigkeit);
		
		//Spezifizieren des Ausführungstatus der Simulation.
		this.setSimulation_ausfuehrungsstatus(false);
		
		/**
		 * Aktivieren des JButton zum Starten der Simulation.
		 * Deaktivieren des JButton zum Pausieren der Simulation.
		 * Deaktivieren des JButton zum Stoppen und Reset der Simulation.
		 */
		this.getController().getView().setJButton_enabled(true, false, false);
		
		/**
		 * Aktivieren der JSlider zum Einstellen 
		 * der Simulationsgeschwindigkeit und der Personenanzahl der Simulation.
		 */
		this.getController().getView().setJSlider_enabled(true, true);
		
		//Hinzufügen des ActionListeners zum JButton zum Starten der Simulation.
		this.getController().getView().getJButton_starten().addActionListener(new ControllerThreadActionListener(this) 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				//Spezifizieren des Ausführungstatus der Simulation.
				this.getControllerThread().setSimulation_ausfuehrungsstatus(true);
		        
				/**
				 * Deaktivieren des JButton zum Starten der Simulation.
				 * Aktivieren des JButton zum Pausieren der Simulation.
				 * Aktivieren des JButton zum Stoppen und Reset der Simulation.
				 */
				this.getControllerThread().getController().getView().setJButton_enabled(false, true, true);
		        
				/**
				 * Aktivieren des JSliders zum Einstellen der Simulationsgeschwindigkeit.
				 * Deaktivieren des JSliders zum Einstellen der Personenanzahl der Simulation.
				 */
				this.getControllerThread().getController().getView().getJSlider_personen().setEnabled(false);
				
				/**
				 * Wenn der Simulationstatus nicht gesetzt, also passiv ist,
				 * werden entsprechende Vorbereitungen dem Neustart entprechend durchgeführt.
				 */
		        if (this.getControllerThread().getSimulation_status() == false)
				{
					//Spezifizieren des Simulationsstatus.
		        	this.getControllerThread().setSimulation_status(true);
					
		        	/**
		        	 * Zurücksetzen des Models um beim Neustarten 
		        	 * der Simulation die richtige Personenanzahl zu verwenden.
		        	 */
		        	this.getControllerThread().getController().getModel().reset(this.getControllerThread().getController().getView().getJSlider_personen().getValue());
					
		        	//Rasterpunkte im Raster entsprechend den Veränderungen im Model umfärben.
		        	this.getControllerThread().getController().getView().getRaster().RasterUmfärben(this.getControllerThread().getController().getModel().getSimulator().getRaster());
				}			
			}
        });
		
		//Hinzufügen des ActionListeners zum JButton zum Pausieren der Simulation.
		this.getController().getView().getJButton_pausieren().addActionListener(new ControllerThreadActionListener(this) {
			@Override
			public void actionPerformed(ActionEvent e) 
			{
				//Spezifizieren des Ausführungstatus der Simulation.
				this.getControllerThread().setSimulation_ausfuehrungsstatus(false);
				
				/**
				 * Aktivieren des JButton zum Starten der Simulation.
				 * Deaktivieren des JButton zum Pausieren der Simulation.
				 * Aktivieren des JButton zum Stoppen und Reset der Simulation.
				 */
				this.getControllerThread().getController().getView().setJButton_enabled(true, false, true);
		    }
        });
		
		//Hinzufügen des ActionListeners zum JButton zum Stoppen und Reset der Simulation.
		this.getController().getView().getJButton_stoppen().addActionListener(new ControllerThreadActionListener(this) 
		{
			@Override
			public void actionPerformed(ActionEvent e) 
			{	
				//Spezifizieren des Simulationsstatus.
				this.getControllerThread().setSimulation_status(false);
				
				//Spezifizieren des Ausführungstatus der Simulation.
				this.getControllerThread().setSimulation_ausfuehrungsstatus(false);
				
				//Zurücksetzen des Controllers
				this.getControllerThread().getController().reset();
		    }
        });
		
		//Hinzufügen des ChangeListeners zum JSlider zum Einstellen der Simulationsgeschwindigkeit.
		this.getController().getView().getJSlider_geschwindigkeit().addChangeListener(new ControllerThreadChangeListener(this) 
		{
			@Override
			public void stateChanged(ChangeEvent e) 
			{
				/**
				 * Spezifizieren der Simulationsgeschwindigkeit 
				 * gemäß dem JSlider zum Einstellen der Simulationsgeschwindigkeit.
				 */
				this.getControllerThread().setSimulation_geschwindigkeit(this.getControllerThread().getController().getView().getJSlider_geschwindigkeit().getValue());
			}
        });
		
		//Hinzufügen des ChangeListeners zum JSlider zum Einstellen der Personenanzahl der Simulation.
		this.getController().getView().getJSlider_personen().addChangeListener(new ControllerThreadChangeListener(this) 
		{
			@Override
			public void stateChanged(ChangeEvent e) 
			{
				/**
				 * Initialisieren des Models mit der Personenanzahl
				 * gemäß dem JSlider zum Einstellen der Personenanzahl.
				 */
				this.getControllerThread().getController().getModel().initialize(this.getControllerThread().getController().getView().getJSlider_personen().getValue());
			}
        });
	}
	
	/**
	 * Starten des ControllerThreads.
	 */
	public void start() 
	{		
		//Undendliche While-Schleife mit einem Try-Catch-Block
		while (true) 
        {
        	try 
        	{
        		/**
        		 * Wenn der Simulationsausführungsstatus gesetzt ist,
        		 * wird die Simulation ausgeführt.        		
        		 */
        		if (this.getSimulation_ausfuehrungsstatus() == true) 
                {
        			//Ausführung eines neuen Simulationsticks.
        			this.getController().tick();
        			
    				// Automatisches Pausieren der Simulation, nachdem keine infizierten Personen mehr vorhanden sind
        			if (this.getController().checkCurrentCountPersonHealth(Health.INFIZIERT, 0) == true)
        			{
        				/**
            			 * Setzen des Simulationsausführungsstatus auf false,
            			 * um die Simulation zu Pausieren.
            			 */
        				this.setSimulation_ausfuehrungsstatus(false);
        				
        				/**
        				 * Deaktivieren des JButton zum Starten der Simulation.
        				 * Deaktivieren des JButton zum Pausieren der Simulation.
        				 * Aktivieren des JButton zum Stoppen und Reset der Simulation.
        				 */
        				this.getController().getView().setJButton_enabled(false, false, true);
        			}
                }
        		
        		/**
    			 * Bewirkt, dass der aktuelle Thread die Ausführung 
    			 * für einen bestimmten Zeitraum anhält.
    			 * Angabe in Millisekunden.
    			 * Abhängigkeit durch die Simulationsgeschwindigkeit gegeben.
    			 */
        		Thread.sleep(400 + 25 * 101 / this.getSimulation_geschwindigkeit());
        	}
        	catch(Exception e) 
        	{
        	}
        }
    }
	
	/**
	 * Gibt den simulation_status zurück
	 *
	 * @return Gibt den simulation_status zurück
	 */
	private boolean getSimulation_status() 
	{
		return simulation_status;
	}

	/**
	 * Setzt den simulation_status.
	 *
	 * @param simulation_status: simulation_status der gesetzt werden soll
	 */
	private void setSimulation_status(boolean simulation_status) 
	{
		this.simulation_status = simulation_status;
	}
	
	/**
	 * Gibt den simulation_ausfuehrungsstatus zurück
	 *
	 * @return Gibt den simulation_ausfuehrungsstatus zurück
	 */
	private boolean getSimulation_ausfuehrungsstatus() 
	{
		return simulation_ausfuehrungsstatus;
	}

	/**
	 * Setzt den simulation_ausfuehrungsstatus
	 *
	 * @param simulations_ausfuehrungsstatus: simulation_ausfuehrungsstatus der gesetzt werden soll
	 */
	private void setSimulation_ausfuehrungsstatus(boolean simulations_ausfuehrungsstatus) 
	{
		this.simulation_ausfuehrungsstatus = simulations_ausfuehrungsstatus;
	}

	/**
	 * Gibt die simulation_geschwindigkeit zurück
	 *
	 * @return Gibt die simulation_geschwindigkeit zurück
	 */
	private int getSimulation_geschwindigkeit() 
	{
		return simulation_geschwindigkeit;
	}

	/**
	 * Setzt die simulation_geschwindigkeit
	 *
	 * @param simulation_geschwindigkeit: simulation_geschwindigkeit die gesetzt werden soll
	 */
	private void setSimulation_geschwindigkeit(int simulation_geschwindigkeit) 
	{
		this.simulation_geschwindigkeit = simulation_geschwindigkeit;
	}
	
	/**
	 * Gibt den Controller zurück
	 *
	 * @return Gibt den Controller zurück
	 */
	private Controller getController() 
	{
		return controller;
	}

	/**
	 * Setzt den controller
	 *
	 * @param controller: controller der gesetzt werden soll
	 */
	private void setController(Controller controller) 
	{
		this.controller = controller;
	}

	/**
	 * Der ActionListener des ControllerThreads
	 */
	private class ControllerThreadActionListener implements ActionListener 
	{
		
		/** 
		 * der controllerThread
		 */
		private ControllerThread controllerThread = null;

		/**
		 * Construktor der Klasse
		 *
		 * @param controllerThread: Objekt vom Typ ControllerThread
		 */
		public ControllerThreadActionListener (ControllerThread controllerThread)
		{
			this.setControllerThread(controllerThread);
		}
		
		/**
		 * Ausführungsmethode
		 *
		 * @param e: Das Argument des Events
		 */
		public void actionPerformed(ActionEvent e) 
		{
	    }

		/**
		 * Gibt den controllerThread zurück
		 *
		 * @return Gibt den controllerThread zurück
		 */
		public ControllerThread getControllerThread() 
		{
			return controllerThread;
		}

		/**
		 * Setzt den controllerThread
		 *
		 * @param controllerThread: controllerThread der gesetzt werden soll
		 */
		private void setControllerThread(ControllerThread controllerThread) 
		{
			this.controllerThread = controllerThread;
		}
	}
	
	/**
	 * ChangeListener der ControllerThread Klasse
	 */
	private class ControllerThreadChangeListener implements ChangeListener 
	{
		
		/** 
		 * Der controllerThread
		 */
		private ControllerThread controllerThread = null;

		/**
		 * Construktor der Klasse
		 *
		 * @param controllerThread: Objekt vom Typ controllerThread
		 */
		public ControllerThreadChangeListener (ControllerThread controllerThread)
		{
			this.setControllerThread(controllerThread);
		}
		
		/**
		 * Die Methode für das Change Event
		 *
		 * @param e: Argumente des Events
		 */
		public void stateChanged(ChangeEvent e) 
		{
		}

		/**
		 * Gibt den controllerThread zurück
		 *
		 * @return Gibt den controllerThread zurück
		 */
		public ControllerThread getControllerThread() 
		{
			return controllerThread;
		}

		/**
		 * Setzt den controllerThread
		 *
		 * @param controllerThread: controllerThread der gesetzt werden soll
		 */
		private void setControllerThread(ControllerThread controllerThread) 
		{
			this.controllerThread = controllerThread;
		}
	}
}