/**
 * The package.
 */
package de.dhbw_heidenheim.pandemie_simulator.controller;

/**
 * The imports.
 */
import de.dhbw_heidenheim.pandemie_simulator.model.Health;
import de.dhbw_heidenheim.pandemie_simulator.model.Model;
import de.dhbw_heidenheim.pandemie_simulator.view.View;

/**
 * Die Klasse welchen den Controller repräsentiert
 */
public class Controller 
{
	
	/** 
	 *  Breite des Rasters
	 */
	private int raster_breite = 0;
	
	/** 
	 *  Höhe des Rasters
	 */
	private int raster_hoehe = 0;
	
	/** 
	 *  Größe der Rasterpunkte
	 */
	private int rasterpunkt_groeße = 0;
	
	/** 
	 *  Die Geschwindigkeit der Simulation. 
	 */
	private int simulation_geschwindigkeit = 0;
	
	/** Objekt vom Typ View */
	private View view = null;
	
	/** Objekt vom Typ Model */
	private Model model = null;
	
	/** Objekt vom Typ ControllerThread */
	private ControllerThread controllerThread = null;
	
	/**
	 * Construktor der Klasse
	 *
	 * @param raster_breite: Breite des Rasters
	 * @param raster_hoehe: Höhe des Rasters
	 * @param rasterpunkt_groeße: Größe der Rasterpunkte
	 * @param simulation_geschwindigkeit: Geschwindigkeit der Simulation
	 */
	public Controller (int raster_breite, int raster_hoehe, int rasterpunkt_groeße, int simulation_geschwindigkeit) 
	{
		//Skalierung des Rasters in der Breite
		this.setRaster_breite(raster_breite);
		
		//Skalierung des Rasters in der Höhe
		this.setRaster_hoehe(raster_hoehe);
		
		//Skalierung des Rasterspunkts in der Breite und in der Höhe.
		this.setRasterpunkt_groeße(rasterpunkt_groeße);
		
		//Spezifizieren der Simulationsgeschwindigkeit.
		this.setSimulation_geschwindigkeit(simulation_geschwindigkeit);
	}
	
	/**
	 * Starten des Controllers. 
	 */
	public void start()
    {
		//Initialisieren der View.
		this.setView(new View(this.getRaster_breite(), this.getRaster_hoehe(), this.getRasterpunkt_groeße()));
		this.getView().initialize();
		
		//Initialisieren des Models.
		this.setModel(new Model(this.getRaster_breite(), this.getRaster_hoehe(), this.getRasterpunkt_groeße()));
		this.getModel().initialize();
		
		//Instanziierung mit Initialisierung und Starten des ControllerThreads.
		this.setControllerThread(new ControllerThread(this, this.getSimulation_geschwindigkeit()));
		this.getControllerThread().start();
    }
	
	/**
	 * Zurücksetzten des Controllers. 
	 */
	public void reset()
	{
		//Zurücksetzten des Models. 
		this.getModel().reset();
		
		//Zurücksetzten der View. 
		this.getView().reset();
	}
	
	/**
	 * Ausführung eines Simulationsticks.
	 */
	public void tick()
	{
		//Ausführung eines Simulationsticks in der Logik des Models.
		this.getModel().getSimulator().nextRound();
		
		/**
		 * Ausführung eines Simulationsticks in der View.
		 * --> beinhaltet:
		 * Rasterpunkte im Raster entsprechend den Veränderungen im Model umfärben.
		 * Funktionsgraphen um die neuen Werte des Simulationsticks ergänzen.
		 * Kennzahlenwerte für die neuen Werte des Simulationsticks anzeigen.
		 */
		this.getView().getRaster().RasterUmfärben(this.getModel().getSimulator().getRaster());
		this.getView().setFunktionsgraph(this.getModel().getKrankheit());
		this.getView().setKennzahlen(this.getModel().getStatistik());
	}
	
	/**
	 * Diese Funktion liefert true zurück, 
	 * wenn die zu überprüfende Zahl genau 
	 * mit der Anzahl der Personen 
	 * des entsprechenden Gesundheitsstatus übereinstimmt.
	 *
	 * @param health: Gesundheitszustand
	 * @param count: Anzahl
	 * @return Ergebnis
	 */
	public boolean checkCurrentCountPersonHealth(Health health, int count)
	{
		return (
				this.getModel().getKrankheit().getData(health).get(
						this.getModel().getKrankheit().getData(health).size() - 1
				) == count
			   );
	}
	
	/**
	 * Gibt die raster_breite zurück
	 *
	 * @return Gibt die raster_breite zurück
	 */
	private int getRaster_breite() 
	{
		return raster_breite;
	}

	/**
	 * Setzt die raster_breite
	 *
	 * @param raster_breite: raster_breite die gesetzt werden soll
	 */
	private void setRaster_breite(int raster_breite) 
	{
		this.raster_breite = raster_breite;
	}
	
	/**
	 * Gibt die raster_hoehe zurück
	 *
	 * @return Gibt die raster_hoehe zurück
	 */
	private int getRaster_hoehe() 
	{
		return raster_hoehe;
	}

	/**
	 * Setzt die raster_hoehe
	 *
	 * @param raster_hoehe: raster_hoehe die gesetzt werden soll
	 */
	private void setRaster_hoehe(int raster_hoehe) 
	{
		this.raster_hoehe = raster_hoehe;
	}

	/**
	 * Gibt die rasterpunkt_groeße zurück
	 *
	 * @return Gibt die rasterpunkt_groeße zurück
	 */
	private int getRasterpunkt_groeße() 
	{
		return rasterpunkt_groeße;
	}

	/**
	 * Setzt die rasterpunkt_groeße
	 *
	 * @param rasterpunkt_groeße: rasterpunkt_groeße die gesetzt werden soll
	 */
	private void setRasterpunkt_groeße(int rasterpunkt_groeße) 
	{
		this.rasterpunkt_groeße = rasterpunkt_groeße;
	}
	
	/**
	 * Gibt die simulation_geschwindigkeit zurück
	 *
	 * @return Gibt die simulation_geschwindigkeit zurück
	 */
	private int getSimulation_geschwindigkeit() 
	{
		return simulation_geschwindigkeit;
	}

	/**
	 * Setzt die simulation_geschwindigkeit.
	 *
	 * @param simulation_geschwindigkeit: simulation_geschwindigkeit die gesetzt werden soll
	 */
	private void setSimulation_geschwindigkeit(int simulation_geschwindigkeit) 
	{
		this.simulation_geschwindigkeit = simulation_geschwindigkeit;
	}

	/**
	 * Gibt die view zurück
	 *
	 * @return Gibt die view zurück
	 */
	protected View getView() 
	{
		return view;
	}

	/**
	 * Setzt die view
	 *
	 * @param view: view die gesetzt werden soll
	 */
	private void setView(View view) 
	{
		this.view = view;
	}
	
	/**
	 * Gibt das model zurück
	 *
	 * @return Gibt das model zurück
	 */
	protected Model getModel() 
	{
		return model;
	}

	/**
	 * Setzt das model
	 *
	 * @param model: model das gesetzt werden soll
	 */
	private void setModel(Model model) 
	{
		this.model = model;
	}

	/**
	 * Gibt den controllerThread zurück
	 *
	 * @return Gibt den controllerThread zurück
	 */
	private ControllerThread getControllerThread() 
	{
		return controllerThread;
	}

	/**
	 * Setzt den controllerThread
	 *
	 * @param controllerThread: controllerThread der gesetzt werden soll
	 */
	private void setControllerThread(ControllerThread controllerThread) 
	{
		this.controllerThread = controllerThread;
	}
}