/**
 * The package.
 */
package de.dhbw_heidenheim.pandemie_simulator;

/**
 * The imports.
 */
import de.dhbw_heidenheim.pandemie_simulator.controller.Controller;

/**
 * Klasse welche die Simulation repräsentiert
 */
public class Simulation 
{
	
	/**
	 *  Stellt die Rasterbreite dar
	 */
	private int raster_breite = 0;
	
	/** 
	 * Stellt die Rasterhöhe dar
	 */
	private int raster_hoehe = 0;
	
	/**
	 * Stellt die Rasterpunkt Größe dar 
	 */
	private int rasterpunkt_groeße = 0;
	
	/**
	 *  Die Geschwindigkeit der Simulation. 
	 */
	private int simulation_geschwindigkeit = 0;
	
	/**
	 *  Das ist der Controller der Simulation. 
	 */
	private Controller controller = null;
	
	/**
	 * Construktor der Klasse
	 *
	 * @param raster_breite: Breite des Rasters
	 * @param raster_hoehe: Höhe des Rasters
	 * @param rasterpunkt_groeße: Größe der Rasterpunkte
	 * @param simulation_geschwindigkeit: Geschwindigkeit der Simulation
	 */
	public Simulation(int raster_breite, int raster_hoehe, int rasterpunkt_groeße, int simulation_geschwindigkeit) 
	{
		//Skalierung des Rasters in der Breite.
		this.setRaster_breite(raster_breite);
		
		//Skalierung des Rasters in der Höhe.
		this.setRaster_hoehe(raster_hoehe);
		
		//Skalierung des Rasterspunkts in der Breite und in der Höhe.
		this.setRasterpunkt_groeße(rasterpunkt_groeße);
		
		//Spezifizieren der Simulationsgeschwindigkeit.
		this.setSimulation_geschwindigkeit(simulation_geschwindigkeit);
	}
	
	/**
	 * Starten der Simulation. 
	 */
	public void start()
	{
		//Instanziierung mit Initialisierung und Starten des Controllers.
		this.setController(new Controller(this.getRaster_breite(), this.getRaster_hoehe(), this.getRasterpunkt_groeße(), this.getSimulation_geschwindigkeit()));
		this.getController().start();
	}
	
	/**
	 * Gibt die raster_breite zurück
	 *
	 * @return Gibt die raster_breite zurück
	 */
	private int getRaster_breite() 
	{
		return raster_breite;
	}

	/**
	 * Setzt die raster_breite
	 *
	 * @param raster_breite: raster_breite die gesetzt werden soll
	 */
	private void setRaster_breite(int raster_breite) 
	{
		this.raster_breite = raster_breite;
	}

	/**
	 * Gibt die raster_hoehe zurück
	 *
	 * @return Gibt die raster_hoehe zurück
	 */
	private int getRaster_hoehe() 
	{
		return raster_hoehe;
	}

	/**
	 * Setzt die raster_hoehe
	 *
	 * @param raster_hoehe: raster_hoehe die gesetzt werden soll
	 */
	private void setRaster_hoehe(int raster_hoehe) 
	{
		this.raster_hoehe = raster_hoehe;
	}

	/**
	 * Gibt die rasterpunkt_groeße zurück
	 *
	 * @return Gibt die rasterpunkt_groeße zurück
	 */
	private int getRasterpunkt_groeße() 
	{
		return rasterpunkt_groeße;
	}

	/**
	 * Setzt die rasterpunkt_groeße.
	 *
	 * @param rasterpunkt_groeße: rasterpunkt_groeße die gesetzt werden soll
	 */
	private void setRasterpunkt_groeße(int rasterpunkt_groeße) 
	{
		this.rasterpunkt_groeße = rasterpunkt_groeße;
	}

	/**
	 * Gibt die simulation_geschwindigkeit zurück
	 *
	 * @return Gibt die simulation_geschwindigkeit zurück
	 */
	private int getSimulation_geschwindigkeit() 
	{
		return simulation_geschwindigkeit;
	}

	/**
	 * Setzt die simulation_geschwindigkeit
	 *
	 * @param simulation_geschwindigkeit: simulation_geschwindigkeit die gesetzt werden soll
	 */
	private void setSimulation_geschwindigkeit(int simulation_geschwindigkeit) 
	{
		this.simulation_geschwindigkeit = simulation_geschwindigkeit;
	}

	/**
	 * Gibt den controller zurück
	 *
	 * @return Gibt den controller zurück
	 */
	private Controller getController() 
	{
		return controller;
	}

	/**
	 * Setzt den Controller
	 *
	 * @param controller: controller der gesetzt werden soll
	 */
	private void setController(Controller controller) 
	{
		this.controller = controller;
	}
}