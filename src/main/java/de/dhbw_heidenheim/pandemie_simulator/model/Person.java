package de.dhbw_heidenheim.pandemie_simulator.model;

/**
 * Klasse die eine Person repräsentiert
 */
public class Person 
{
	
	/** id der Person */
	private int id;
	
	/** Geschwindigkeit in X Richtung */
	private int speedX;
	
	/** Geschwindigkeit in Y Richtung */
	private int speedY;
	
	/** Gesundheitszustand der Person */
	private Health health;
	
	/** Anzahl der Krankheitstage */
	private int anzahlKrankheitstage;
	
	/** Anzahl der Totestage */
	private int anzahlTotestage;

	/**
	 * Konstruktor der Klasse Person
	 *
	 * @param id: id der Person
	 * @param speedX: Geschwindigkeit in X-Richtung
	 * @param speedY: Geschwindigkeit in Y-Richtung
	 * @param health: Gesundheitszustand der Person
	 */
	public Person(int id, int speedX, int speedY, Health health) 
	{
		this.setId(id);
		this.setSpeedX(speedX);
		this.setSpeedY(speedY);
		this.setHealth(health);
	}
	
	/**
	 * Gibt die id zurück
	 *
	 * @return Gibt die id zurück
	 */
	public int getId() 
	{
		return id;
	}

	/**
	 * Setzt die id
	 *
	 * @param id: id die gesetzt werden soll
	 */
	public void setId(int id) 
	{
		this.id = id;
	}

	/**
	 * Gibt speedX zurück
	 *
	 * @return Gibt speedX zurück
	 */
	public int getSpeedX() 
	{
		return speedX;
	}
	
	/**
	 * Setzt speedX
	 *
	 * @param speedX: speedX welches gesetzt werden soll
	 */
	public void setSpeedX(int speedX) 
	{
		this.speedX = speedX;
	}
	
	/**
	 * Gibt speedY zurück
	 *
	 * @return Gibt speedY zurück
	 */
	public int getSpeedY() 
	{
		return speedY;
	}
	
	/**
	 * Setzt speedY
	 *
	 * @param speedY: speedY welches gesetzt werden soll
	 */
	public void setSpeedY(int speedY) 
	{
		this.speedY = speedY;
	}
	
	/**
	 * Gibt den Gesundheitszustand zurück
	 *
	 * @return Gibt den Gesundheitszustand zurück
	 */
	public Health getHealth() 
	{
		return health;
	}
	
	/**
	 * Setzt den Gesundheitszustand
	 *
	 * @param health: Gesundheitszustand der gesetzt werden soll
	 */
	public void setHealth(Health health) 
	{
		this.health = health;
	}
	
	/**
	 * Gibt anzahlKrankheitstage zurück
	 *
	 * @return Gibt anzahlKrankheitstage zurück
	 */
	public int getAnzahlKrankheitstage() 
	{
		return anzahlKrankheitstage;
	}

	/**
	 * Setzt die anzahlKrankheitstage
	 *
	 * @param anzahlKrankheitstage: anzahlKrankheitstage die gesetzt werden soll
	 */
	public void setAnzahlKrankheitstage(int anzahlKrankheitstage) 
	{
		this.anzahlKrankheitstage = anzahlKrankheitstage;
	}

	/**
	 * Gibt die anzahlTotestage zurück
	 *
	 * @return Gibt die anzahlTotestage zurück
	 */
	public int getAnzahlTotestage() 
	{
		return anzahlTotestage;
	}

	/**
	 * Setzt die anzahlTotestage
	 *
	 * @param anzahlTotestage: anzahlTotestage die gesetzt werden soll
	 */
	public void setAnzahlTotestage(int anzahlTotestage) 
	{
		this.anzahlTotestage = anzahlTotestage;
	}
}