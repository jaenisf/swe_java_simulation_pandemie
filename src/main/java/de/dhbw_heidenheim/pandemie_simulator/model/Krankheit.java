package de.dhbw_heidenheim.pandemie_simulator.model;

import java.util.List;
import java.util.ArrayList;

/**
 * Die Klasse, welche die Krankheit repräsentiert
 */
public class Krankheit 
{	
	
	/** Liste der gesunden Personen */
	private List<Long> gesund = null;
	/** Anzahl der Personen mit dem Zustand Gesund */
	private long anzahl_gesund_gesamt = 0;
	
	/** Liste der infizierten Personen */
	private List<Long> infiziert = null;
	/** Anzahl der Personen mit dem Zustand Infiziert */
	private long anzahl_infiziert_gesamt = 0;
	
	/** Liste der immunen Personen */
	private List<Long> immun = null;
	/** Anzahl der Personen mit dem Zustand Immun */
	private long anzahl_immun_gesamt = 0;
	
	/** Liste der toten Personen */
	private List<Long> tot = null;
	/** Anzahl der Personen mit dem Zustand tot */
	private long anzahl_tot_gesamt = 0;
	
	/** Liste der neu infizierten */
	private List<Long> neuInfiziert = null;
	
	/**
	 * Konstruktor
	 */
	public Krankheit() 
	{
		
	}
	
	/**
	 * Zurücksetzen aller Daten
	 */
	public void reset()
	{
		this.resetData(Health.GESUND);
		this.resetData(Health.INFIZIERT);
		this.resetData(Health.IMMUN);
		this.resetData(Health.TOT);
	}
	
	/**
	 * Zurücksetzen der Health entsprechenden Daten
	 *
	 * @param health: der Zustand, dessen Daten zurückgesetzt werden sollen
	 */
	public void resetData(Health health) 
	{
		switch(health)
		{
			case GESUND:
				this.setGesund(new ArrayList<>());
				this.anzahl_gesund_gesamt = 0;
				break;
			case INFIZIERT:
				this.setInfiziert(new ArrayList<>());
				this.anzahl_infiziert_gesamt = 0;
				break;
			case IMMUN:
				this.setImmun(new ArrayList<>());
				this.anzahl_immun_gesamt = 0;
				break;
			case TOT:
				this.setTot(new ArrayList<>());
				this.anzahl_tot_gesamt = 0;
				break;
		}	
	}
	
	/**
	 * Fügt Daten zu der Health entsprechenden Liste hinzu
	 *
	 * @param health: Zustand zu dem der count hinzugefügt werden soll
	 * @param count: Zahl die hinzugefügt werden soll
	 */
	public void addData(Health health, long count) 
	{
		try
		{
			switch(health)
			{
				case GESUND:
					if(this.getGesund() != null)
					{
						this.getGesund().add(count);
					}
					break;
				case INFIZIERT:
					if(this.getInfiziert() != null)
					{
						this.getInfiziert().add(count);
					}
					break;
				case IMMUN:
					if(this.getImmun() != null)
					{
						this.getImmun().add(count);
					}
					break;
				case TOT:
					if(this.getTot() != null)
					{
						this.getTot().add(count);
					}
					break;
			}
		}
		catch(Exception ex)
		{
			
		}
	}
	
	/**
	 * Erhöhen der Zähler für die jeweiligen Gesamtzahlen
	 * 
	 * @param health: Zustand, dessen Gesamtzahl erhöht werden soll
	 * @param count: Wert um den Erhöht werden soll
	 */
	public void AddGesamtData(Health health, long count)
	{
		switch(health)
		{
			case GESUND:
				this.anzahl_gesund_gesamt = this.anzahl_gesund_gesamt + count;
				break;
			case INFIZIERT:
				this.anzahl_infiziert_gesamt = this.anzahl_infiziert_gesamt + count;
				break;
			case IMMUN:
				this.anzahl_immun_gesamt = this.anzahl_immun_gesamt + count;
				break;
			case TOT:
				this.anzahl_tot_gesamt = this.anzahl_tot_gesamt + count;
				break;
		}
	}
	
	/**
	 * Gibt die jeweilige zum Health-Zustand passende Liste zurück
	 *
	 * @param health: Zustand, zu dem die Liste zurückgegeben werden soll
	 * @return Gibt die passende Liste zurück
	 */
	public List<Long> getData(Health health)
	{
		switch(health)
		{
			case GESUND:
				return this.getGesund();
			case INFIZIERT:
				return this.getInfiziert();
			case IMMUN:
				return this.getImmun();
			case TOT:
				return this.getTot();
		}
		
		return null;
	}
	
	/**
	 * Gibt die zum Health-Zustand passende Gesamtzahl zurück
	 * 
	 * @param health: Zustand: zu dem die Gesamtzahl zurückgegeben werden soll
	 * @return Gibt die jeweilige Gesamtzahl zurück
	 */
	public long GetAnzahlGesamt(Health health)
	{
		switch(health)
		{
			case GESUND:
				return this.anzahl_gesund_gesamt;
			case INFIZIERT:
				return this.anzahl_infiziert_gesamt;
			case IMMUN:
				return this.anzahl_immun_gesamt;
			case TOT:
				return this.anzahl_tot_gesamt;
		}
		
		return 0;
	}
	
	/**
	 * Fügt die übergebene Zahl zu der Liste der neuInfizierten hinzu
	 *
	 * @param count: Wert der hinzugefügt werden soll
	 */
	protected void addNeuInfiziert(long count)
	{
		this.getNeuInfiziert().add(count);
	}

	/**
	 * Gibt die Liste der gesunden Personen zurück
	 *
	 * @return Liste der Gesunden
	 */
	private List<Long> getGesund() 
	{
		return gesund;
	}

	/**
	 * Setzt die Liste der gesunden
	 *
	 * @param gesund: Liste die gesetzt werden soll
	 */
	private void setGesund(List<Long> gesund) 
	{
		this.gesund = gesund;
	}

	/**
	 * Gibt die Liste der infizierten Personen zurück
	 *
	 * @return Liste der infizierten
	 */
	private List<Long> getInfiziert() 
	{
		return infiziert;
	}

	/**
	 * Setzt die Liste der infizierten
	 *
	 * @param infiziert: Liste die gesetzt werden soll
	 */
	private void setInfiziert(List<Long> infiziert) 
	{
		this.infiziert = infiziert;
	}

	/**
	 * Gibt die Liste der immunen Personen zurück
	 *
	 * @return Liste der immunen
	 */
	private List<Long> getImmun() 
	{
		return immun;
	}

	/**
	 * Setzt die Liste der immunen
	 *
	 * @param immun: Liste die gesetzt werden soll
	 */
	private void setImmun(List<Long> immun) 
	{
		this.immun = immun;
	}

	/**
	 * Gibt die Liste der toten Personen zurück
	 *
	 * @return Liste der toten
	 */
	private List<Long> getTot() 
	{
		return tot;
	}

	/**
	 * Setzt die Liste der toten
	 *
	 * @param tot: Liste die gesetzt werden soll
	 */
	private void setTot(List<Long> tot) 
	{
		this.tot = tot;
	}

	/**
	 * Gibt die Liste der neu infizierten Personen zurück
	 *
	 * @return Liste der neu infizierten
	 */
	protected List<Long> getNeuInfiziert() 
	{
		return neuInfiziert;
	}

	/**
	 * Setzt die Liste der neu infizierten
	 *
	 * @param neuInfiziert: Liste die gesetzt werden soll
	 */
	protected void setNeuInfiziert(List<Long> neuInfiziert) 
	{
		this.neuInfiziert = neuInfiziert;
	}
}