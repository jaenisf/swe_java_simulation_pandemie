package de.dhbw_heidenheim.pandemie_simulator.model;

/**
 * Dieses Enum repräsentiert den Gesundheitszustand
 */
public enum Health 
{
	/** Zustand: Gesund */
	GESUND,
	
	/** Zustand: Infiziert */
	INFIZIERT,
	
	/** Zustand: Immun */
	IMMUN,
	
	/** Zustand: Tot */
	TOT;
}