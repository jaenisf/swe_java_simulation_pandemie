package de.dhbw_heidenheim.pandemie_simulator.model;

/**
 * Klasse die das Model repräsentiert
 */
public class Model 
{
	
	/** Die Breite des Rasters */
	private int raster_breite = 0;
	
	/** Die Höhe des Rasters */
	private int raster_hoehe = 0;
	
	/** Die Größe eines Rasterpunktes */
	private int rasterpunkt_groeße = 0;
	
	/** Objekt der Klasse Krankheit */
	private Krankheit krankheit = null;
	
	/** Objekt der Klasse Statistik */
	private Statistik statistik = null;
	
	/** Objekt der Klasse Simulator */
	private Simulator simulator = null;
	
	/**
	 * Konstruktor der Klasse
	 *
	 * @param raster_breite: Breite des Rasters
	 * @param raster_hoehe: Höhe des Rasters
	 * @param rasterpunkt_groeße: Größe des Rasterpunktes
	 */
	// Konstruktor
	public Model(int raster_breite, int raster_hoehe, int rasterpunkt_groeße)
	{
		this.setRaster_breite(raster_breite);
		this.setRaster_hoehe(raster_hoehe);
		this.setRasterpunkt_groeße(rasterpunkt_groeße);
	}
	
	/**
	 * Model initialisieren
	 */
	public void initialize()
	{
		this.initialize(0);
	}

	/**
	 * Model initialisieren
	 *
	 * @param anzahl_personen: Anzahl der eingestellten Personen
	 */
	public void initialize(int anzahl_personen)
	{
		// Initialisiert Krankheit
		this.setKrankheit(new Krankheit());
		
		// Initialisiert Statistik
		this.setStatistik(new Statistik(this.getKrankheit(), anzahl_personen));
		
		// Initialisiert Simulator
		this.setSimulator(new Simulator(this.getRaster_breite(), this.getRaster_hoehe(), anzahl_personen, this.getStatistik()));
		this.getSimulator().start();
	}
	
	/**
	 * Model zurücksetzten
	 */
	public void reset()
	{
		this.reset(0);
	}
	
	/**
	 * Model zurücksetzten
	 *
	 * @param anzahlPersonen: Anzahl der eingestellten Personen
	 */
	public void reset(int anzahlPersonen)
	{
		this.getKrankheit().reset();
		this.getStatistik().reset();
		this.getSimulator().reset(anzahlPersonen);
	}
	
	/**
	 * Gibt die raster_breite zurück
	 *
	 * @return gibt die raster_breite zurück
	 */
	private int getRaster_breite() 
	{
		return raster_breite;
	}

	/**
	 * Setzt die raster_breite
	 *
	 * @param raster_breite: Die zu setzende raster_breite
	 */
	private void setRaster_breite(int raster_breite) 
	{
		this.raster_breite = raster_breite;
	}

	/**
	 * Gibt die raster_hoehe zurück
	 *
	 * @return gibt die raster_hoehe zurück
	 */
	private int getRaster_hoehe() 
	{
		return raster_hoehe;
	}

	/**
	 * Setzt die raster_hoehe
	 *
	 * @param raster_hoehe: raster_hoehe die gesetzt werden soll
	 */
	private void setRaster_hoehe(int raster_hoehe) 
	{
		this.raster_hoehe = raster_hoehe;
	}

	/**
	 * Gibt die rasterpunkt_groeße zurück
	 *
	 * @return Gibt die rasterpunkt_groeße zurück
	 */
	private int getRasterpunkt_groeße() 
	{
		return rasterpunkt_groeße;
	}

	/**
	 * Setzt die rasterpunkt_groeße
	 *
	 * @param rasterpunkt_groeße: rasterpunkt_groeße die gesetzt werden soll
	 */
	private void setRasterpunkt_groeße(int rasterpunkt_groeße) 
	{
		this.rasterpunkt_groeße = rasterpunkt_groeße;
	}
	
	/**
	 * Gibt das Objekt krankheit zurück
	 *
	 * @return Gibt das Objekt krankheit zurück
	 */
	public Krankheit getKrankheit() 
	{
		return krankheit;
	}

	/**
	 * Setzt das Objekt Krankheit
	 *
	 * @param krankheit: Objekt vom Typ Krankheit das gesetzt werden soll
	 */
	private void setKrankheit(Krankheit krankheit) 
	{
		this.krankheit = krankheit;
	}

	/**
	 * Gibt das Objekt statistik zurück
	 *
	 * @return Gibt das Objekt statistik zurück
	 */
	public Statistik getStatistik() 
	{
		return statistik;
	}

	/**
	 * Setzt das Objekt statistik
	 *
	 * @param statistik: Objekt vom Typ Statistik das gesetzt werden soll
	 */
	private void setStatistik(Statistik statistik) 
	{
		this.statistik = statistik;
	}

	/**
	 * Gibt das Objekt simulator zurück
	 *
	 * @return Gibt das Objekt simulator zurück
	 */
	public Simulator getSimulator() 
	{
		return simulator;
	}

	/**
	 * Setzt das Objekt simulator
	 *
	 * @param simulator: Objekt vom Typ Simulator das gesetzt werden soll
	 */
	private void setSimulator(Simulator simulator) 
	{
		this.simulator = simulator;
	}	
}