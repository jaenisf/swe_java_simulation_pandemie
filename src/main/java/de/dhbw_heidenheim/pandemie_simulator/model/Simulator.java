package de.dhbw_heidenheim.pandemie_simulator.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasse welche den Simulator repräsentiert
 */
public class Simulator 
{
	
	/** Die Größe in X-Richtung */
	private int sizeX;
	
	/** Die Größe in Y-Richtung */
	private int sizeY;
	
	/** Die Anzahl der Personen */
	private int anzahlPersonen;
	
	/** Die Liste der Personen */
	private List<Person> personen;
	
	/** Das Raster */
	private List<Person>[][] raster;
	
	/** Objekt vom Typ Statistik */
	private Statistik statistik;
	
	/**
	 * Construktor der Klasse
	 *
	 * @param sizeX: Größe in X-Richtung
	 * @param sizeY: Größe in Y-Richtung
	 * @param anzahlPersonen: Anzahl der Personen
	 * @param statistik: Objekt vom Typ Statistik
	 */
	public Simulator(int sizeX, int sizeY, int anzahlPersonen, Statistik statistik) 
	{
		this.sizeX = sizeX;
		this.sizeY = sizeY;
		this.anzahlPersonen = anzahlPersonen;
		this.statistik = statistik;
	}
	
	/**
	 * Simulator starten    
	 */
    public void start()
    {
    	//Zurücksetzen der Daten
        this.statistik.resetData(this.anzahlPersonen);
    	
        //Personen Generieren
    	this.personen = this.createPersonen();
    	
    	//Analysieren der Daten
    	this.analyzeData();
    	
    	//Erstellen des Rasters
        this.raster = createArraylist();
        
        //Befüllen des Rasters
        this.raster = fill(this.raster, personen);
        
        //System.out.println("Startzustand:");
        //tostringFirst(this.raster);
    }
    
    /**
     * Simulator zurücksetzen   
     *
     * @param anzahlPersonen: anzahlPersonen die gesetzt werden soll
     */
    public void reset(int anzahlPersonen)
    {
        this.anzahlPersonen = anzahlPersonen;
    	
        //Zurücksetzen der Daten
    	this.statistik.resetData(this.anzahlPersonen);
    	
    	//Erstellen der Personen
    	this.personen = this.createPersonen();
    	
    	//Daten analysieren
    	this.analyzeData();
    	
    	//Erstellen der Array-Liste
        this.raster = createArraylist();
        
        //Füllen des Rasters
        this.raster = fill(this.raster, personen);
    }
    
    /**
     * Nächste Runde / Nächster Tick
     */
    public void nextRound() 
	{
    	this.nextRound(0);
	}

	/**
	 * Nächster Runde / Nächster Tick
	 *
	 * @param durchgangsnummer: Nummer des aktuellen Durchlaufes
	 */
	public void nextRound(int durchgangsnummer) 
	{
		//Bewegen der Personen und entfernen der toten Personen
	    this.raster = moveAllPersonAndRemoveDeadPerson(this.raster);
	    
	    //System.out.println(durchgangsNummer + "te move:");
	    //tostring(this.raster);
	    
	    //Ermitteln ob kranke Gesund werden oder sterben
	    this.managePersonsHealth(this.personen);
	    
	    //Ermitteln ob sich Personen infizieren
	    this.raster = checkInfiziert(this.raster);
	    
	    //Daten analysieren
	    this.analyzeData();
	    
	    //System.out.println(durchgangsNummer + "ter Durchgang:\n"
	    //			    + "Letalität: " + this.getStatistik().berechneLetalität(durchgangsNummer)
	    //				+ "\nInzidenz: " + this.getStatistik().berechneInzidenz(durchgangsNummer)
	    //				+ "\nPrävalenz: "+ this.getStatistik().berechnePrävalenz(durchgangsNummer));
	    //System.out.println(durchgangsNummer + "te Infizierung:");
	    //tostring(this.raster);
	}
	
	/**
	 * Erstellt die Liste der Personen
	 *
	 * @return Gibt die erstellte Personenliste zurück
	 */
	private List<Person> createPersonen() 
	{
		List<Person> personen = new ArrayList<>();
		
    	int anzahlPersonenInfiziert = calculateRandomInfectedCount();
    	
    	for(int i = 0; i < anzahlPersonen; i++) 
    	{
			int speedX = calculateRandomSpeed(this.sizeX);
			int speedY = calculateRandomSpeed(this.sizeY);
			
			Health health = Health.GESUND;
			
			if(i < anzahlPersonenInfiziert) 
			{
				health = Health.INFIZIERT;
			}
			
			personen.add(new Person(i, speedX, speedY, health));
    	}
    	
    	return personen;
    }

	/**
	 * Analysieren der Daten
	 */
	private void analyzeData() 
	{
		for(Health health : Health.values()) 
		{
			this.statistik.analyzeData(health, this.personen);
		}
	}
	
	/**
	 * Ermittelt wie viele Personen am Anfang erkrankt sind
	 *
	 * @return Gibt den ermittelten Zahlenwert zurück
	 */
	private int calculateRandomInfectedCount() 
	{
		return (int)(Math.random() * (anzahlPersonen / 5000)) + 1;
	}
	
	/**
	 * Ermittelt eine Zufällige Positionskoordinate
	 *
	 * @param size: Länge des Rasters
	 * @return Gibt die ermittelte Position zurück
	 */
	private int calculateRandomPosition(int size) 
	{
		return (int)(Math.random() * size);
	}
	
	/**
	 * Ermittelt eine zufällige Geschwindigkeit
	 *
	 * @param size: Länge des Rasters
	 * @return Gibt die ermittelte Geschwindigkeit zurück
	 */
	private int calculateRandomSpeed(int size)
	{
		return (int)(((Math.random()*2) - 1) * Math.ceil(size * 0.025));
	}
	
	/**
	 * Ermittelt einen zufälligen Gesundheitszustand
	 *
	 * @return Gibt den ermittelten Wert zurück
	 */
	private int calculateRandomHealthState() 
	{
		return (int)(Math.random() * 1001) - 500;
	}

	/**
	 * Erstellen der Array-Liste
	 *
	 * @return Gibt die erstellte Liste zurück
	 */
	private List<Person>[][] createArraylist() 
	{	
		List<Person>[][] raster = new ArrayList[sizeX][sizeY];
		
    	for(int x = 0; x < raster.length; x++) 
    	{
    		for(int y = 0; y < raster[0].length; y++) 
    		{
    				raster[x][y] = new ArrayList<Person>();
			}
		}
    	
    	return raster;
	}
		
	/**
	 * Füllt das Raster
	 *
	 * @param raster: Das Raster
	 * @param personen: Liste der Personen
	 * @return Das gefüllte Raster wird zurück gegeben
	 */
	private List<Person>[][] fill(List<Person>[][] raster, List<Person> personen) 
	{
    	personen.parallelStream()
			.forEach(p -> {
	    		int positionx = calculateRandomPosition(raster.length);
	    		int positiony = calculateRandomPosition(raster[0].length);
	    		
	    		raster[positionx][positiony].add(p);
    	});
    	return raster;
    }
	
    /**
     * Gibt die Daten der Personen der Personenliste aus
     *
     * @param raster: Das Raster der Personen
     */
    private void tostringFirst(List<Person>[][] raster) 
    {
    	for(int x = 0; x < raster.length; x++)
    	{
    		for(int y = 0; y < raster[0].length; y++)
    		{
    			if(!raster[x][y].isEmpty()) 
    			{
	    			for(Person p : raster[x][y]) 
	    			{
	    					System.out.println("x:" + x + " y: " + y + " ID: " + p.getId()
				    							+ " Health: " + p.getHealth().toString() 
				    							+ " SpeedX: " + p.getSpeedX()
				    							+ " SpeedY: " + p.getSpeedY());
    				}
    			}
			}
		}
    	
    	System.out.println("\n\n\n");
	}
    
    /**
     * Gibt die Daten der Personen der Personenliste aus
     *
     * @param raster: Das Raster der Personen
     */
    private void tostring(List<Person>[][] raster) 
    {
    	for(int x = 0; x < raster.length; x++)
    	{
    		for(int y = 0; y < raster[0].length; y++)
    		{
    			if(!raster[x][y].isEmpty()) 
    			{
	    			for(Person p : raster[x][y])
	    			{
	    					System.out.println("x:" + x + " y: " + y + " ID: " + p.getId()
	    										+ " Health: " + p.getHealth().toString());
    				}
    			}
			}
		}
    	
    	System.out.println("\n\n");
	}
	
	/**
	 * Bewegt die Personen und entfernt die toten Personen
	 *
	 * @param raster: Das Raster
	 * @return Gibt das überarbeitete Raster wieder zurück
	 */
	private List<Person>[][] moveAllPersonAndRemoveDeadPerson(List<Person>[][] raster) 
	{
		List<Person>[][] rasterCopy = this.createArraylist();
		
		for(int x = 0; x < raster.length; x++)
		{
    		for(int y = 0; y < raster[0].length; y++)
    		{
    			if(!raster[x][y].isEmpty()) 
    			{
	    			for(Person p : raster[x][y]) 
	    			{
    					if(p.getHealth() == Health.TOT) 
    					{
    						if(p.getAnzahlTotestage() <= 1 + Math.random() * 2) 
    						{
	    						rasterCopy[x][y].add(p);
		    					p.setAnzahlTotestage(p.getAnzahlTotestage() + 1);
    						}
    					} 
    					else 
    					{
    						int newPosX = calculateNewPosition(x, p.getSpeedX(), raster.length);
	    					int newPosY = calculateNewPosition(y, p.getSpeedY(), raster[x].length);
	    					
	    					p.setSpeedX(calculateRandomSpeed(raster.length));
	    					p.setSpeedY(calculateRandomSpeed(raster[x].length)); 
	    					rasterCopy[newPosX][newPosY].add(p);
    					}
    				}
    			}
			}
		}
		
		/* sequentielles Arbeiten ist schneller
		 * Zeit sequentielles Arbeiten: 293ms
		 * Zeit paralleles Arbeiten: 343ms 
		 
		IntStream.range(0, raster.length)					
			.forEach(x -> {
				IntStream.range(0, raster[x].length)
				 	.forEach(y -> {
				 		if(!raster[x][y].isEmpty()) {
				 			raster[x][y]
					 			.stream()
								.forEach(p -> {
			    					if(p.getHealth() == Health.TOT && p.getAnzahlTotestage() <= 5) 
			    					{
			    						rasterCopy[x][y].add(p);
				    					p.setAnzahlTotestage(p.getAnzahlTotestage()+1);
			    					} else {
			    						int newPosX = calculateNewPosition(x, p.getSpeedX(), raster.length);
				    					int newPosY = calculateNewPosition(y, p.getSpeedY(), raster[x].length);
				    					
				    					p.setSpeedX(calculateRandomSpeed(raster.length));
				    					p.setSpeedY(calculateRandomSpeed(raster[x].length)); 
				    					
				    					rasterCopy[newPosX][newPosY].add(p);
			    					}
								});
				 		}
				 });
			});*/
		
		return rasterCopy;
	}

	/**
	 * Ermittelt die neue Position
	 *
	 * @param position: Die Koordinate
	 * @param move: Die Geschwindigkeit in Koordinatenrichtung
	 * @param length: Länge des Rasters
	 * @return Gibt den ermittelten Wert wieder zurück
	 */
	private int calculateNewPosition(int position, int move, int length) 
	{
		int newPosition = move + position;
		
		if(length - 1 < newPosition) 
		{
			return newPosition - length;
		} 
		else if( 0 > newPosition) 
		{
			return 0 - newPosition;
		}
		else 
		{
			return newPosition;
		}
	}

	/**
	 * Ermittelt ob kranke Personen gesund werden oder sterben
	 *
	 * @param personen: Liste der Personen
	 */
	private void managePersonsHealth(List<Person> personen) 
	{
		personen.parallelStream()
			.forEach(p -> {
				if(p.getHealth() == Health.INFIZIERT) 
				{
					int random = this.calculateRandomHealthState();
					int random2 = 499 - p.getAnzahlKrankheitstage();
					
					if(random <= -random2) 
					{
						random = -1;
					} 
					else if(random >= random2)
					{
						random = 1;
					} 
					else 
					{
						random = 0;
					}
					
					switch(random) 
					{
						case -1:
							p.setHealth(Health.TOT);
							this.statistik.getKrankheit().AddGesamtData(Health.TOT, 1);
							p.setAnzahlTotestage(1);
							break;
						case 0:
							p.setAnzahlKrankheitstage(p.getAnzahlKrankheitstage() + 1);
							break;
						case 1:
							p.setHealth(Health.IMMUN);
							this.statistik.getKrankheit().AddGesamtData(Health.IMMUN, 1);
							break;
					}
				}
			});
	}
	
	/**
	 * Prüft ob sich eine Person infiziert hat und infiziert Sie gegebenfalls
	 *
	 * @param raster: Das Raster
	 * @return Gibt das angepasste Raster wieder zurück
	 */
	private List<Person>[][] checkInfiziert(List<Person>[][] raster) 
	{
		for(int x = 0; x < raster.length; x++) 
		{
			for(int y = 0; y < raster[0].length; y++) 
			{
				if(!raster[x][y].isEmpty()) 
				{
	    			for(Person p : raster[x][y]) 
	    			{
    					if(p.getHealth() == Health.INFIZIERT || p.getHealth() == Health.TOT) 
    					{
    						for(Person p2 : raster[x][y]) 
    						{
    							/*int gesund = this.calculateRandomHealthState();
    							int krank = this.calculateRandomHealthState();*/
    							
    							if(p2.getHealth() == Health.GESUND && (Math.random() + Math.random()) * 0.5 > Math.random() * Math.random() /*System.currentTimeMillis() % 2 == 0*/) 
    							{
    								p2.setHealth(Health.INFIZIERT);
    								this.statistik.getKrankheit().AddGesamtData(Health.INFIZIERT, 1);
    							}
    						}
    						
    						continue;
    					}
					}
				}
			}
		}
		
		/*
		 * sequentielles Arbeiten ist schneller, kaum ein Unterschied
		 * Zeit sequentielles Arbeiten: 435ms
		 * Zeit paralleles Arbeiten: 445ms 
		 * IntStream.range(0, raster.length)
			.parallel()
			.forEach(x -> {
				IntStream.range(0, raster[x].length)
				 	.forEach(y -> {
				 		if(!raster[x][y].isEmpty()) {
				 			raster[x][y]
					 			.stream()
								.forEach(p -> {
									if(p.getHealth() == Health.INFISZIERT) {
			    						raster[x][y].stream().forEach(p2 -> {
			    							if(p2.getHealth() == Health.GESUND) {
			    								p2.setHealth(Health.INFISZIERT);
			    							}
			    						});
			    						return;
			    					}
								});
				 		}
				 });
			});*/
		
		return raster;
	}
	
	/**
	 * Gibt die Liste der Personen zurück
	 *
	 * @return Gibt die Liste der Personen zurück
	 */
	public List<Person> getPersonen() 
	{
		return personen;
	}

	/**
	 * Gibt das Raster zurück
	 *
	 * @return Gibt das Raster zurück
	 */
	public List<Person>[][] getRaster() 
	{
		return raster;
	}
	
    /**
     * Gibt das Objekt vom Typ Statistik zurück
     *
     * @return Gibt das Objekt vom Typ Statistik zurück
     */
    public Statistik getStatistik() 
    {
		return statistik;
	}
}