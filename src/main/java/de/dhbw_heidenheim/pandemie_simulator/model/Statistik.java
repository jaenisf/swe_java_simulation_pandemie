package de.dhbw_heidenheim.pandemie_simulator.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Klasse die die Statistik repräsentiert
 */
public class Statistik 
{
	
	/** Objekt vom Typ Krankheit */
	private Krankheit krankheit;
	
	/** Die Anzahl der Personen */
	private int anzahlPersonen;
	
	/**
	 * Construktor der Klasse
	 *
	 * @param krankheit: Objekt vom Typ Krankheit
	 * @param anzahlPersonen: Anzahl der Personen
	 */
	public Statistik(Krankheit krankheit, int anzahlPersonen) 
	{
		this.krankheit = krankheit;
		this.anzahlPersonen = anzahlPersonen;
	}
	
	/**
	 * Simulator zurücksetzen   
	 */
    public void reset()
    {
    	this.anzahlPersonen = 0;
    }
	
	/**
	 * Analysieren der Daten
	 *
	 * @param health: Gesundheitszustand
	 * @param personen: Liste der Personen
	 */
	public void analyzeData(Health health, List<Person> personen) 
	{
		Krankheit krankheit = this.getKrankheit();
		long data = personen.parallelStream()
				  .filter(p -> p.getHealth() == health)
				  .count();
		
		if(krankheit != null)
		{
			krankheit.addData(health, data);
		}
		
		if(health == Health.GESUND) {
			this.berechneNeuInfiziert();
		}
	}

	/**
	 * Daten zurücksetzen
	 *
	 * @param anzahlPersonen: Anzahl der Personen
	 */
	public void resetData(int anzahlPersonen) {
		for(Health health : Health.values()) 
		{
			this.getKrankheit().resetData(health);
		}
		
		this.anzahlPersonen = anzahlPersonen;
		this.resetNeuInfiziert();
	}
	
	/**
	 * Gibt krankheit zurück
	 *
	 * @return Gibt krankheit zurück
	 */
	public Krankheit getKrankheit() 
	{
		return krankheit;
	}
	
	/**
	 * Setzt die Krankheit
	 *
	 * @param krankheit: Krankheit die gesetzt werden soll
	 */
	public void setKrankheit(Krankheit krankheit) 
	{
		this.krankheit = krankheit;
	}
	
	/**
	 * Berechnet die Morbidität
	 * Zahl der an einer bestimmten Krankheit leidenden Personen
	 * pro Zahl der Gesamtbevölkerung in einem Zeitraum
	 *
	 * @return Gibt den errechneten Prozentwert zurück
	 */
	public float berechneMorbiditaet()
	{
		try
		{	
			float infiziert = this.getKrankheit().GetAnzahlGesamt(Health.INFIZIERT);
			if(this.anzahlPersonen > 0)
			{
				float erg = (infiziert/this.anzahlPersonen)*100;
				return erg;
			}
			else
			{
				return 0;
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return 0;
		}
	}
	
	/**
	 * Berechnet die Letalität
	 * Zahl der an einer Krankheit gestorbenen, pro Zahl der an dieser Krankheit erkrankten
	 *
	 * @return Gibt den ermittelten Prozentwert zurück
	 */
	public float berechneLetalitaet()
	{
		try
		{
			float infiziert = this.getKrankheit().GetAnzahlGesamt(Health.INFIZIERT);
			float tot = this.getKrankheit().GetAnzahlGesamt(Health.TOT);
			
			if(infiziert > 0)
			{
				float erg = (tot / infiziert) * 100;
				return erg;
			}
			else
			{
				return 0;
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
			return 0;
		}
	}
	
	/**
	 * Zurücksetzen der Neu infizierten
	 */
	private void resetNeuInfiziert() 
	{
		try
		{
			this.getKrankheit().setNeuInfiziert(new ArrayList<Long>());
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
	
	/**
	 * Berechnen der Neu infizierten
	 */
	private void berechneNeuInfiziert() 
	{
		try
		{
			int size = this.getKrankheit().getData(Health.GESUND).size();
			
			if(size <= 1) 
			{
				this.getKrankheit().addNeuInfiziert(this.anzahlPersonen - this.getKrankheit().getData(Health.GESUND).get(size-1));
			} 
			else 
			{
				this.getKrankheit().addNeuInfiziert(this.getKrankheit().getData(Health.GESUND).get(size-2) - this.getKrankheit().getData(Health.GESUND).get(size-1));
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
}