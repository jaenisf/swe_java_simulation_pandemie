package de.dhbw_heidenheim.pandemie_simulator;

import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

import de.dhbw_heidenheim.pandemie_simulator.model.*;

// TODO: Auto-generated Javadoc
/**
 * The Class SimulatorTest.
 */
public class SimulatorTest 
{
	
	/**
	 * Simulation test.
	 * Misst die Zeit einer nicht visuellen Simulation.
	 */
	@Test
	public void simulationTest()
	{	

		
		long start = System.currentTimeMillis();
		this.simulationWithArrayTest();
		long finish = System.currentTimeMillis();
		
		long timeElapsed = finish - start;
		
		System.out.println("Zeit sequentielles Arbeiten: " + timeElapsed + "ms\n");
	
	}
	
	
	/**
	 * Simuliert den Krankheitsverlauf ohne Frontend und testet ihn auf Funktionsfähigkeit.
	 */
	private void simulationWithArrayTest() 
	{	
		Krankheit krankheit = new Krankheit();
		Statistik statistik = new Statistik(krankheit, 5000);
		Simulator simulator = new Simulator(50, 50, 5000, statistik);
		
		simulator.reset(5000);
		
		assertTrue(simulator.getPersonen() != null && simulator.getPersonen().isEmpty() == false);
		
		this.checkRasterExist(simulator.getRaster());
		
		for(int i = 0; i < 100; i++) 
		{
			simulator.nextRound(i);
			
			for(Health health : Health.values()) 
			{
				System.out.println(health + ":\n" 
						   + simulator.getStatistik().getKrankheit().getData(health));
			}
		}
		
		/* Test das reset funktioniert
		 * 
		 * simulator.resetRaster();
		 *
		 * simulator.nextRound(0);
		 * 
		 * for(Health health : Health.values()) 
		 * {
		 * 	 System.out.println(health + ":\n" +	simulator.getStatistik().getKrankheit().getData(health)+ "\n");
		 * }
		 * */
	}
	
	/**
	 * Check raster exist.
	 *
	 * @param raster the raster
	 */
	private void checkRasterExist(List<Person>[][] raster) 
	{
		assertTrue(raster != null && raster.length != 0);
		
		for(List<Person>[] rasterRow : raster) 
		{
			assertTrue(rasterRow != null && rasterRow.length != 0);
			
			for(List<Person> rasterElement : rasterRow) 
			{
				assertTrue(rasterElement != null);
			}
		}
	}
}