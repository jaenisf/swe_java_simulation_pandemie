package de.dhbw_heidenheim.pandemie_simulator;

import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;

// TODO: Auto-generated Javadoc
/**
 * Unit test for simple App.
 */
public class AppTest 
{
    
    /**
     * Should answer with true.
     */
    @Test
    public void shouldAnswerWithTrue()
    {
    	int[] array = {23, 43, 55, 12};
    	  
    	int[] copiedArray = array.clone();
    	
    	assertArrayEquals(copiedArray, array);
    	array[0] = 9;
    	 
    	assertTrue(copiedArray[0] != array[0]);
    }
}